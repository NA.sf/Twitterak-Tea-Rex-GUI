#ifndef SETTINGWINDOW_HPP
#define SETTINGWINDOW_HPP

#include <QMainWindow>
#include <QLineEdit>
#include <QGroupBox>

#include "mytextedit.hpp"

#include "back/Twitterak.hpp"

namespace Ui {
class SettingWindow;
}

class LoggedInWindow;

class SettingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit SettingWindow(QWidget *parent = nullptr, /*LoggedInWindow *loggedInWindow = nullptr,*/ Twitterak *twitterak = nullptr, User* currentUser = nullptr);
    void changeStyleFilled(const QString &text);
//    void setErrorStyle(const QString&, QLineEdit* line = nullptr, QGroupBox* group = nullptr);
    ~SettingWindow();

private:
    Ui::SettingWindow *ui;
    MyTextEdit * bioTextEdit;
//    LoggedInWindow *loggedInWindow; // for when user wants to go back to main window
    Twitterak *twitterak;
    User* currentUser;
    QString profile{""};

protected:
    bool eventFilter(QObject *obj, QEvent *event) override; // for setting profile

    void closeEvent(QCloseEvent *) override;
//    void keyPressEvent(QKeyEvent *e) override;

private slots:
//    void handleIndexChanged(int index); // limits datas needed to the user type
//    void on_btnSettingWindow_clicked();
    void on_btnLoggedOut_clicked();
    void on_btnDelete_clicked();
    void on_btnEdit_clicked();
};

#endif // SettingWindow_HPP
