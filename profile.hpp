#ifndef PROFILE_HPP
#define PROFILE_HPP

#include <QMainWindow>
#include "back/User.hpp"

#include "back/Twitterak.hpp"

namespace Ui {
class Profile;
}

class LoggedInWindow;

class Profile : public QMainWindow
{
    Q_OBJECT

public:
    explicit Profile(QWidget *parent = nullptr, User* searchedUser = nullptr, LoggedInWindow* loggedInWindow = nullptr, Twitterak * twitterak = nullptr, User * currentUser = nullptr);
    ~Profile();
    void updateFollow();

protected:
    void closeEvent(QCloseEvent *) override;

private slots:
    void on_btnFollow_clicked();

private:
    Ui::Profile *ui;
    User * searchedUser;
    User * currentUser;
    LoggedInWindow * loggedInWindow;
    Twitterak* twitterak;
};

#endif // PROFILE_HPP
