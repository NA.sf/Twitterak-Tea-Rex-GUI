QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    back/Anonymous.cpp \
    back/Config.cpp \
    back/Date.cpp \
    back/Hashtag.cpp \
    back/Mention.cpp \
    back/Organization.cpp \
    back/Personal.cpp \
    back/Tweet.cpp \
    back/Twitterak.cpp \
    back/User.cpp \
    back/hash-library/crc32.cpp \
    back/hash-library/digest.cpp \
    back/hash-library/keccak.cpp \
    back/hash-library/md5.cpp \
    back/hash-library/sha1.cpp \
    back/hash-library/sha256.cpp \
    back/hash-library/sha3.cpp \
    clickablelabel.cpp \
    loggedinwindow.cpp \
    profile.cpp \
    settingwindow.cpp \
    signup.cpp \
    login.cpp \
    main.cpp \
    mainwindow.cpp \
    mytextedit.cpp \
    toolbuttons.cpp

HEADERS += \
    back/Anonymous.hpp \
    back/Config.hpp \
    back/Date.hpp \
    back/Hashtag.hpp \
    back/Mention.hpp \
    back/Organization.hpp \
    back/Personal.hpp \
    back/Tweet.hpp \
    back/Twitterak.hpp \
    back/User.hpp \
    back/hash-library/crc32.h \
    back/hash-library/hash.h \
    back/hash-library/hmac.h \
    back/hash-library/keccak.h \
    back/hash-library/md5.h \
    back/hash-library/sha1.h \
    back/hash-library/sha256.h \
    back/hash-library/sha3.h \
    clickablelabel.hpp \
    loggedinwindow.hpp \
    profile.hpp \
    settingwindow.hpp \
    signup.hpp \
    login.hpp \
    mainwindow.hpp \
    mytextedit.hpp \
    toolbuttons.hpp

FORMS += \
    loggedinwindow.ui \
    profile.ui \
    settingwindow.ui \
    signup.ui \
    login.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES += \
    back/hash-library/LICENSE \
    back/hash-library/readme.md
