#include "profile.hpp"
#include "ui_profile.h"

#include <QFile>
#include <QScrollBar>
#include <QBitmap>
#include <QPainter>
#include <QMessageBox>
#include "loggedinwindow.hpp"

#include <QDebug>

Profile::Profile(QWidget *parent, User* searchedUser, LoggedInWindow* loggedInWindow, Twitterak* twitterak, User * currentUser) :
    QMainWindow(parent),
    ui(new Ui::Profile),
    searchedUser(searchedUser),
    currentUser(currentUser),
    loggedInWindow(loggedInWindow),
    twitterak(twitterak)
{
    ui->setupUi(this);

    this->ui->lblUsername->setText(QString::fromStdString(searchedUser->getUsername()));
    this->ui->lblName->setText(QString::fromStdString(searchedUser->getName()));
    if (searchedUser->getWork() != nullptr) this->ui->lblCEO->setText(QString::fromStdString(searchedUser->getWork()->getUsername()));
    this->ui->lblCountry->setText(QString::fromStdString(searchedUser->getCountry()));
    this->ui->lblHttps->setText(QString::fromStdString(searchedUser->getLink()));
    this->ui->lblBiography->setText(QString::fromStdString(searchedUser->getBiography()));
    ui->lblShowFollower->setText(QString::number(searchedUser->getFollowers().size()));
    ui->lblShowFollowing->setText(QString::number(searchedUser->getFollowings().size()));
    ui->tweetBox->setFixedSize(1151, 2000);
    QWidget *widget = new QWidget();
    widget->setLayout(new QVBoxLayout);
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->tweetBox->setLayout(new QVBoxLayout);
    widget->layout()->addWidget(ui->tweetBox);
    ui->scrollArea->setWidget(widget);
    ui->scrollArea->verticalScrollBar()->setStyleSheet(
        "QScrollBar:vertical{border: none;background-color: rgb(164, 195, 178);width: 8px;margin: 0px 0 0px 0;border-radius: 0px;}"
        "QScrollBar::handle:vertical{background-color: rgb(107, 144, 128);min-height: 30px;border-radius: 4px;}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical, QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {width: 0px;}");
    ui->scrollArea->setStyleSheet("border: none;");
    if      (searchedUser->getHeader() == "White")  ui->lblColor->setStyleSheet("background-color: rgb(255, 255, 252)");
    else if (searchedUser->getHeader() == "Black")  ui->lblColor->setStyleSheet("background-color: rgb(  0,   0,   3)");
    else if (searchedUser->getHeader() == "Orange") ui->lblColor->setStyleSheet("background-color: rgb(255, 214, 165)");
    else if (searchedUser->getHeader() == "Pink")   ui->lblColor->setStyleSheet("background-color: rgb(255, 198, 255)");
    else if (searchedUser->getHeader() == "Red")    ui->lblColor->setStyleSheet("background-color: rgb(255, 173, 173)");
    else if (searchedUser->getHeader() == "Blue")   ui->lblColor->setStyleSheet("background-color: rgb(155, 246, 255)");
    else if (searchedUser->getHeader() == "Green")  ui->lblColor->setStyleSheet("background-color: rgb(202, 255, 191)");
    else if (searchedUser->getHeader() == "Purple") ui->lblColor->setStyleSheet("background-color: rgb(189, 178, 255)");
    else if (searchedUser->getHeader() == "Yellow") ui->lblColor->setStyleSheet("background-color: rgb(253, 255, 182)");
    if (searchedUser->getName() == "Anonymous User") this->ui->profile->setPixmap(QPixmap(":/img/img/anonymous.png"));
    else if(QFile::exists("../Files/Profiles/" + QString::number(this->searchedUser->getID())))
    {
        QPixmap pixmap("../Files/Profiles/" + QString::number(this->searchedUser->getID()));
        int size = qMin(pixmap.width(), pixmap.height());
        pixmap = pixmap.copy((pixmap.width() - size) / 2, (pixmap.height() - size) / 2, size, size);
        QBitmap mask(pixmap.size());
        QPainter painter(&mask);
        painter.fillRect(mask.rect(), Qt::color0);
        painter.setBrush(Qt::color1);
        painter.drawEllipse(mask.rect());
        pixmap.setMask(mask);
        pixmap = pixmap.scaled(ui->profile->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        this->ui->profile->setPixmap(pixmap);
    }
}

Profile::~Profile()
{
    delete ui;
}


void Profile::closeEvent(QCloseEvent *event)
{
    QMainWindow::closeEvent(event);
    this->close();
    this->deleteLater();
    this->loggedInWindow->show();
}

void Profile::on_btnFollow_clicked()
{
    try
    {
        if (this->searchedUser != nullptr) this->searchedUser->setFollower(this->currentUser);
        if (this->currentUser != nullptr) this->currentUser->setFollowing(this->searchedUser);
        this->updateFollow();
        this->loggedInWindow->updateFollow();
    }
    catch (const std::invalid_argument& ex)
    {
        QMessageBox::critical(this, "Error!", ex.what());
    }
}

void Profile::updateFollow()
{
    if (this->searchedUser != nullptr) this->ui->lblShowFollower->setText(QString::number(searchedUser->followersCount()));
}
