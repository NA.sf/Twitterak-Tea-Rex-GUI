#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QCloseEvent>

#include "signup.hpp"
#include "login.hpp"
#include "back/Twitterak.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, Twitterak * twitterak = nullptr);
    void resizeEvent(QResizeEvent *event) override;
    void closeEvent(QCloseEvent *event) override;
    ~MainWindow();

private slots:
    void on_btnLogin_clicked(); // redirect user to login window
    void on_btnSignup_clicked(); // reditect user to sign up window

private:
    Ui::MainWindow *ui;
    Signup * signupUi; // for when user wants to go back to sign up window
    Login * loginUi;// for when user wants to go back to login window
    Twitterak * twitterak;
};
#endif // MAINWINDOW_HPP
