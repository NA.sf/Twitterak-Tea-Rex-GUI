#include "loggedinwindow.hpp"
#include "ui_loggedinwindow.h"
#include "profile.hpp"
#include "back/User.hpp"
#include "clickablelabel.hpp"
#include "mytextedit.hpp"
#include "toolbuttons.hpp"
#include "settingwindow.hpp"
#include "back/Mention.hpp"

#include <QToolButton>
#include <QDialog>
#include <QVBoxLayout>
#include <QDebug>
#include <QMessageBox>
#include <QScrollArea>
#include <QScrollBar>
#include <QBitmap>
#include <QPainter>
#include "back/Config.hpp"

LoggedInWindow::LoggedInWindow(QWidget *parent, User* currentUser, Twitterak* twitterak, Hashtag* hashtag) :
    QMainWindow(parent),
    ui(new Ui::LoggedInWindow),
    currentUser(currentUser),
    twitterak(twitterak)
{
    ui->setupUi(this);
    ui->lblShowFollower->setText(QString::number(this->currentUser->followersCount()));
    ui->lblShowFollowing->setText(QString::number(this->currentUser->followingsCount()));
    ui->lblShowFollower->installEventFilter(this); ///to let the label when is pushed happened something
    ui->lblShowFollowing->installEventFilter(this); ///to let the label when is pushed happened something
    hashtag  = new Hashtag();
    this->ui->lblUsername->setText(QString::fromStdString(currentUser->getUsername()));
    this->ui->lblName->setText(QString::fromStdString(currentUser->getName()));
    if (currentUser->getWork() != nullptr) this->ui->lblCEO->setText(QString::fromStdString(currentUser->getWork()->getUsername()));
    this->ui->lblCountry->setText(QString::fromStdString(currentUser->getCountry()));
    this->ui->lblHttps->setText( QString::fromStdString(currentUser->getLink()));
    this->ui->lblBiography->setText(QString::fromStdString(currentUser->getBiography()));
    ui->tweetBox->setFixedSize(1151, 2000);
    tweetTextEdit = new MyTextEdit(ui->tweetBox, 0, true);
    tweetTextEdit->move(40, 505);
    tweetTextEdit->setFixedSize(1070, 200);
    tweetTextEdit->setStyleSheet("background-color: rgb(164, 195, 178);\ncolor : rgb(246, 255, 248);");
    QWidget *widget = new QWidget();
    widget->setLayout(new QVBoxLayout);
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->tweetBox->setLayout(new QVBoxLayout);
    widget->layout()->addWidget(ui->tweetBox);
    ui->scrollArea->setWidget(widget);
    ui->scrollArea->verticalScrollBar()->setStyleSheet(
        "QScrollBar:vertical{border: none;background-color: rgb(164, 195, 178);width: 8px;margin: 0px 0 0px 0;border-radius: 0px;}"
        "QScrollBar::handle:vertical{background-color: rgb(107, 144, 128);min-height: 30px;border-radius: 4px;}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical, QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {width: 0px;}");
    ui->scrollArea->setStyleSheet("border: none;");
    if      (currentUser->getHeader() == "White")  ui->lblColor->setStyleSheet("background-color: rgb(255, 255, 252)");
    else if (currentUser->getHeader() == "Black")  ui->lblColor->setStyleSheet("background-color: rgb(  0,   0,   3)");
    else if (currentUser->getHeader() == "Orange") ui->lblColor->setStyleSheet("background-color: rgb(255, 214, 165)");
    else if (currentUser->getHeader() == "Pink")   ui->lblColor->setStyleSheet("background-color: rgb(255, 198, 255)");
    else if (currentUser->getHeader() == "Red")    ui->lblColor->setStyleSheet("background-color: rgb(255, 173, 173)");
    else if (currentUser->getHeader() == "Blue")   ui->lblColor->setStyleSheet("background-color: rgb(155, 246, 255)");
    else if (currentUser->getHeader() == "Green")  ui->lblColor->setStyleSheet("background-color: rgb(202, 255, 191)");
    else if (currentUser->getHeader() == "Purple") ui->lblColor->setStyleSheet("background-color: rgb(189, 178, 255)");
    else if (currentUser->getHeader() == "Yellow") ui->lblColor->setStyleSheet("background-color: rgb(253, 255, 182)");
    if (currentUser->getName() == "Anonymous User") this->ui->profile->setPixmap(QPixmap(":/img/img/anonymous.png"));
    else if(QFile::exists("../Files/Profiles/" + QString::number(this->currentUser->getID())))
    {
        QPixmap pixmap("../Files/Profiles/" + QString::number(this->currentUser->getID()));
        int size = qMin(pixmap.width(), pixmap.height());
        pixmap = pixmap.copy((pixmap.width() - size) / 2, (pixmap.height() - size) / 2, size, size);
        QBitmap mask(pixmap.size());
        QPainter painter(&mask);
        painter.fillRect(mask.rect(), Qt::color0);
        painter.setBrush(Qt::color1);
        painter.drawEllipse(mask.rect());
        pixmap.setMask(mask);
        pixmap = pixmap.scaled(ui->profile->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        this->ui->profile->setPixmap(pixmap);
    }
}

LoggedInWindow::~LoggedInWindow()
{
    delete ui;
}


/////////////////////////////////////slot function for open profile
void LoggedInWindow::new_Window()
{
    qDebug() << "done";
    ClickableLabel *label = qobject_cast<ClickableLabel*>(sender());
    User* searchedUser = twitterak->getUser(label->text().toStdString());
    Profile *another(new Profile(this, searchedUser, this, this->twitterak, this->currentUser));
    this->hide();
    another->show();
}
/////////////////////////////////////

bool LoggedInWindow::eventFilter(QObject *watched, QEvent *event)
{
    qDebug() << "here in event filter of loggedin" << event->type();
    if (watched == ui->lblShowFollower && event->type() == QEvent::MouseButtonPress)
    {
        qDebug() << "here in if of event filter\n";
        QDialog *dialog = new QDialog(this);
        dialog->setFixedSize(800, 500);
        QWidget *widget = new QWidget(dialog);
        widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        widget->setLayout(new QVBoxLayout);
        for(int a{0}; a < this->currentUser->followersCount() ; ++a)
        {
            qDebug() << "here in for ofevent filter\n";
            ClickableLabel *label1 = new ClickableLabel(widget);
            label1->setText(QString::fromStdString(this->currentUser->getFollowers()[a]->getUsername()));
            label1->move(0, a * 50);
            widget->layout()->addWidget(label1);
            connect(label1, SIGNAL(clicked()), this, SLOT(new_Window()));
        }
        dialog->show();
        return true; // The event is handled
    }
    else if (watched == ui->lblShowFollowing && event->type() == QEvent::MouseButtonPress)
    {
        qDebug() << "here in if of event filter\n";
        QDialog *dialog = new QDialog(this);
        dialog->setFixedSize(800, 500);
        QWidget *widget = new QWidget(dialog);
        widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        widget->setLayout(new QVBoxLayout);
        for(int a{0}; a < this->currentUser->followingsCount() ; ++a)
        {
            qDebug() << "here in for ofevent filter\n";
            ClickableLabel *label1 = new ClickableLabel(widget);
            label1->setText(QString::fromStdString(this->currentUser->getFollowings()[a]->getUsername()));
            label1->move(0, a * 50);
            widget->layout()->addWidget(label1);
            connect(label1, SIGNAL(clicked()), this, SLOT(new_Window()));
        }
        dialog->show();
        return true; // The event is handled
    }
    return false; // The event is not handled
}

void LoggedInWindow::on_btnTweet_clicked()
{
    this->tweetTextEdit->fixText();
    try
    {
        this->currentUser->setVec(this->tweetTextEdit->toPlainText().toStdString());
        this->tweetTextEdit->clear();
    }
    catch(const std::invalid_argument& ex)
    {
        QMessageBox::critical(this, "Error", ex.what());
    }
}

void LoggedInWindow::closeEvent(QCloseEvent *event)
{
    twitterak->save();
    qDebug() << "Closing LoggedInWindow";
    QMainWindow::closeEvent(event);
    event->accept();
    this->close();
    qDebug() << "after Closing LoggedInWindow";
}

void LoggedInWindow::like(bool checked)
{
    ToolButtons* Toolbutton = qobject_cast<ToolButtons*>(sender());
    qDebug() << Toolbutton;
    if (Toolbutton)
    {
        qDebug() << "in if in toolbutton";
        if (!checked)
        {
            qDebug() << "in if in checked";
            Toolbutton->getTweet()->setLike(currentUser);
            Toolbutton->setIcon(QIcon(":/img/img/like.png"));
        }
        else
        {
            qDebug() << "in else in checked";
            Toolbutton->getTweet()->seDislike(currentUser);
            Toolbutton->setIcon(QIcon(":/img/img/dislike.png"));
        }
    }
}

void LoggedInWindow::on_btnSetting_clicked()
{
    SettingWindow * settingwWindow = new SettingWindow(nullptr, this->twitterak, this->currentUser);
    settingwWindow->show();
    this->deleteLater();
//    this->closeEvent(new QCloseEvent());
}

void LoggedInWindow::updateFollow()
{
    if (this->currentUser != nullptr) this->ui->lblShowFollowing->setText(QString::number(this->currentUser->followingsCount()));
}

void LoggedInWindow::on_btnSearchHashtag_clicked()
{
    std::string check = (this->ui->lineSearchHashtag->text()).toStdString();

    hashtag = hashtag->checkHashtag(check);
    if(hashtag != nullptr)
    {
        QDialog *dialog = new QDialog(this);
        QWidget *widget = new QWidget(dialog);
        widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        dialog->setFixedSize(800, 600);
        widget->setLayout(new QVBoxLayout);
        for (int i{0}; i < hashtag->getSizeOfVector(); ++i)
        {
            QGroupBox *groupbox = new QGroupBox(widget);
            groupbox->move(0 , i*100);
            groupbox->setFixedSize(700 , 100);
            QLabel *labelTweet = new QLabel(QString::fromStdString(hashtag->getTweet(i)), groupbox);
            QLabel *labelTweeter = new QLabel(QString::fromStdString(hashtag->getTweeter(i)), groupbox);
            QPushButton *Pushbutton = new QPushButton(groupbox);
            Tweet* tweet = hashtag->Tweets(i);
            User* theUser = this->currentUser;
            connect(Pushbutton, &QPushButton::clicked, dialog, [dialog, tweet, theUser](bool checked)
                    {
                        QDialog *newwindow = new QDialog(dialog);
                        newwindow->setFixedSize(700,300);
                        MyTextEdit* MentionTextEdit;
                        MentionTextEdit = new MyTextEdit(newwindow, 0, true);
                        MentionTextEdit->move(40, 50);
                        MentionTextEdit->setFixedSize(500, 200);
                        MentionTextEdit->setStyleSheet("background-color: rgb(164, 195, 178);\ncolor : rgb(246, 255, 248);");
                        QPushButton *Pushbutton = new QPushButton(newwindow);
                        Pushbutton->move(40, 250);
                        Pushbutton->setFixedSize(50, 50);
                        connect(Pushbutton, &QPushButton::clicked, newwindow, [MentionTextEdit, tweet, theUser](bool checked)
                                {
                                    MentionTextEdit->fixText();
                                    //Mention* mention = new Mention((MentionTextEdit->toPlainText().toStdString()),currentUser);
                                    tweet->setMention(new Mention((MentionTextEdit->toPlainText().toStdString()), theUser));
                                    MentionTextEdit->clear();
                                });
                        newwindow->show();
                    });
            ToolButtons *Toolbutton = new ToolButtons(groupbox,hashtag->Tweets(i));
            Toolbutton->setFixedSize(30,30);
            //Toolbutton->setStyleSheet("background-color: red;");
            Toolbutton->setCheckable(true);
            if (hashtag->Tweets(i)->checkLike(currentUser))
            {
                Toolbutton->setIcon(QIcon(":/img/img/like.png"));
                Toolbutton->toggled(true);
            }
            else
            {
                Toolbutton->setIcon(QIcon(":/img/img/dislike.png"));
                Toolbutton->toggled(false);
            }

            //            qDebug() << Toolbutton->;
            connect(Toolbutton, SIGNAL(toggled(bool)), this, SLOT(like(bool)));
            Toolbutton->move(20, 70);
            Pushbutton->move(70, 70);
            Pushbutton->setIcon(QIcon(":/img/img/mention.png"));
            labelTweet->move(0, 50);
            labelTweeter->move(0, 0);
            widget->layout()->addWidget(groupbox);
        }
        dialog->show();
    }
    else
    {
        this->ui->lineSearchHashtag->setText("");
        QMessageBox::critical(this, "Error!", " ! There is no tweet with this hashtag.");
    }
}

void LoggedInWindow::on_btnSearchAccount_clicked()
{
    std::string searchText = (ui->lineSearchAccount->text()).toStdString();
    if (searchText == this->currentUser->getUsername()) this->ui->btnSearchAccount->setText("");
    else if(twitterak->getUser(searchText) != nullptr)
    {
        Profile* profile = new Profile(this, twitterak->getUser(searchText), this, this->twitterak, this->currentUser);
        this->hide();
        profile->show();
    }
    else
    {
        this->ui->lineSearchAccount->setText("");
        QMessageBox::critical(this, "Error!", " ! User Not Found!");
    }
}
