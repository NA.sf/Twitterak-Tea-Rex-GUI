#include "signup.hpp"
#include "ui_signup.h"

#include "mainwindow.hpp"

#include "back/Config.hpp"
#include "loggedinwindow.hpp"

#include "back/User.hpp"


#include <QFileDialog>
#include <QBitmap>
#include <QPainter>
#include <QPainterPath>
#include <QScrollArea>
#include <QScrollBar>
#include <QDebug>
#include <QLineEdit>
#include <QRegularExpressionValidator>
#include <QGuiApplication>
#include <QScreen>
#include <QVBoxLayout>
#include <QMessageBox>
#include <iostream>

Signup::Signup(QWidget *parent, MainWindow *mainWindow, Twitterak *twitterak) :
    QMainWindow(parent),
    ui(new Ui::Signup),
    mainWindow(mainWindow),
    twitterak(twitterak)

{
    ui->setupUi(this);
    setFixedHeight(QGuiApplication::primaryScreen()->geometry().height() - 100);
    QWidget *widget = new QWidget();
    QPushButton *btnBack = new QPushButton("", this);
    btnBack->setStyleSheet("image: url(:/img/img/backButton.png);border: none;");
    btnBack->setFixedSize(51, 51);
    btnBack->move(10, 10);
    btnBack->setCursor(Qt::PointingHandCursor);
    widget->setLayout(new QVBoxLayout);
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->groupBox->setLayout(new QVBoxLayout);
    bioTextEdit = new MyTextEdit(ui->groupBox);
    widget->layout()->addWidget(ui->groupBox);
    ui->scrollArea->setWidget(widget);
    ui->scrollArea->verticalScrollBar()->setStyleSheet(
        "QScrollBar:vertical{border: none;background-color: rgb(164, 195, 178);width: 8px;margin: 0px 0 0px 0;border-radius: 0px; font-color: black;}"
        "QScrollBar::handle:vertical{background-color: rgb(107, 144, 128);min-height: 30px;border-radius: 4px;}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical, QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {width: 0px;}");
    ui->scrollArea->setStyleSheet("border: none;");
    ui->dateEdit->setDateRange(QDate(1902, 1, 1), QDate::currentDate());
    connect(ui->toolButton, &QToolButton::toggled, this, [this](bool checked)
    {
        if(checked)
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeClose.png"));
            ui->linePassword->setEchoMode(QLineEdit::Normal);
        }
        else
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeOpen.png"));
            ui->linePassword->setEchoMode(QLineEdit::Password);
        }
    });
    ui->comboHeader->setItemData(0, QColor(255, 255, 252), Qt::ForegroundRole);
    ui->comboHeader->setItemData(1, QColor(  0,   0,   3), Qt::ForegroundRole);
    ui->comboHeader->setItemData(2, QColor(255, 214, 165), Qt::ForegroundRole);
    ui->comboHeader->setItemData(3, QColor(255, 198, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(4, QColor(255, 173, 173), Qt::ForegroundRole);
    ui->comboHeader->setItemData(5, QColor(155, 246, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(6, QColor(202, 255, 191), Qt::ForegroundRole);
    ui->comboHeader->setItemData(7, QColor(189, 178, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(8, QColor(253, 255, 182), Qt::ForegroundRole);
    bioTextEdit->setLimitation(160);
    bioTextEdit->setFixedSize(429, 151);
    bioTextEdit->setWordWrapMode(QTextOption::WrapAnywhere);
    bioTextEdit->setEnabled(false);
    bioTextEdit->setStyleSheet("QTextEdit:enabled{background-color: rgb(246, 255, 248); border: 2px solid black; border-radius: 20px;}"
                               "QTextEdit:disabled{background-color: rgb(246, 255, 248); border: 2px solid gray; border-radius: 20px;}");
    ui->unnecessaryLines->setEnabled(false);
    ui->necessaryLines->setEnabled(false);
    connect(ui->comboTypes, SIGNAL(currentIndexChanged(int)), this, SLOT(handleIndexChanged(int))); // to limit user's required field based on type
    ui->profile->installEventFilter(this);
    ui->linePhone->setValidator(new QRegularExpressionValidator(QRegularExpression("^989[0-9]{9}"), this)); // validates phone number to accepts 989xxxxxxxxx
    ui->lineUsername->setValidator(new QRegularExpressionValidator(QRegularExpression("[A-Za-z][A-Za-z0-9]*"), this)); // validates username

    bioTextEdit->move((ui->groupBox->width() - bioTextEdit->width()) / 2, ui->unnecessaryLines->y() + ui->unnecessaryLines->height());
    connect(btnBack, &QPushButton::clicked, this, [this]() {
        this->hide();
        this->mainWindow->show();
    });

    connect(ui->linePassword, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->lineUsername, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->lineName, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->linePhone, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->lineWork, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->lineCountry, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
    connect(ui->lineLink, &QLineEdit::textChanged, this, &Signup::changeStyleFilled);
}


void Signup::handleIndexChanged(int index)  // to limit user's required field based on type
{
    if (this->ui->comboTypes->findText("...") != -1)
    {
        this->ui->comboTypes->removeItem(this->ui->comboTypes->findText("..."));
        --index;
    }
    ui->btnSignup->setEnabled(true);
    emit ui->linePassword->textChanged(ui->linePassword->text());
    emit ui->lineUsername->textChanged(ui->lineUsername->text());
    emit ui->lineName->textChanged(ui->lineName->text());
    emit ui->linePhone->textChanged(ui->linePhone->text());
    emit ui->lineWork->textChanged(ui->lineWork->text());
    emit ui->lineCountry->textChanged(ui->lineCountry->text());
    emit ui->lineLink->textChanged(ui->lineLink->text());
    if (index == 0) // chosen item of comboTypes is Personal
    {
        this->ui->unnecessaryLines->setEnabled(true);
        this->ui->necessaryLines->setEnabled(true);
        if (this->ui->lineName->displayText() == "Anonymous User") this->ui->lineName->clear();
        this->ui->lblWork->setText("Organization");
        this->bioTextEdit->setEnabled(true);
        this->ui->dateEdit->setEnabled(true);
        this->ui->lblBirthDate->setEnabled(true);
        this->bioTextEdit->setLimitation(160);
        this->ui->profile->setEnabled(true);
        this->ui->profile->setPixmap(QPixmap(":/img/img/profile.png"));
    }
    else if (index == 1) // chosen item of comboTypes is Anonymous
    {
        this->ui->necessaryLines->setEnabled(true);
        this->ui->lineName->setText("Anonymous User");
        this->ui->unnecessaryLines->setEnabled(false);
        this->bioTextEdit->setEnabled(false);
        this->ui->profile->setPixmap(QPixmap(":/img/img/anonymous.png"));
        this->ui->profile->setEnabled(false);
    }
    else if (index == 2) // chosen item of comboTypes is Organization
    {
        this->ui->unnecessaryLines->setEnabled(true);
        this->ui->necessaryLines->setEnabled(true);
        if (this->ui->lineName->displayText() == "Anonymous User") this->ui->lineName->clear();
        this->ui->lblWork->setText("CEO");
        this->bioTextEdit->setEnabled(true);
        this->ui->dateEdit->setEnabled(false);
        this->ui->lblBirthDate->setEnabled(false);
        this->bioTextEdit->setLimitation(1100);
        this->ui->profile->setEnabled(true);
        this->ui->profile->setPixmap(QPixmap(":/img/img/profile.png"));
    }
}

Signup::~Signup()
{
    qDebug() << "here for signup";
    delete ui;
}

void Signup::closeEvent(QCloseEvent *event)
{
    this->hide(); // hide the current window
    this->mainWindow->show();
}

bool Signup::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == this->ui->profile && event->type() == QEvent::MouseButtonPress && this->ui->profile->isEnabled() == true)
    {
        profile = QFileDialog::getOpenFileName(this, tr("Open Image"), "../", tr("Image Files (*.png *.jpg *.bmp)"));
        if (!profile.isEmpty())
        {
            if (!QDir("../Files/Profiles/").exists())
            {
                QDir("../Files/Profiles/").mkpath(".");
            }
            QPixmap pixmap(profile);
            int size = qMin(pixmap.width(), pixmap.height());
            pixmap = pixmap.copy((pixmap.width() - size) / 2, (pixmap.height() - size) / 2, size, size);
            QBitmap mask(pixmap.size());
            QPainter painter(&mask);
            painter.fillRect(mask.rect(), Qt::color0);
            painter.setBrush(Qt::color1);
            painter.drawEllipse(mask.rect());
            pixmap.setMask(mask);
            pixmap = pixmap.scaled(ui->profile->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
            this->ui->profile->setPixmap(pixmap);
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }
}

void Signup::on_btnSignup_clicked()
{
    if (this->ui->lineUsername->text().isEmpty())
    {
        setErrorStyle(" ! This feild is required.", this->ui->lineUsername);
        return;
    }
    else if (ui->lineUsername->text().length() < 5)
    {
        setErrorStyle(" ! Too short for a username.", this->ui->lineUsername);
        return;
    }
    if (this->ui->linePassword->text().isEmpty())
    {
        setErrorStyle(" ! This feild is required.", this->ui->linePassword, this->ui->passwordBox);
        return;
    }
    else if (this->ui->linePassword->text().length() < 6)
    {
        setErrorStyle(" ! Too short for a password.", this->ui->linePassword, this->ui->passwordBox);
        return;
    }
    if ((this->ui->comboTypes->currentIndex() == 0) || (this->ui->comboTypes->currentIndex() == 2)) // for personal and organization account
    {
        if (this->ui->lineName->text().isEmpty())
        {
            setErrorStyle(" ! This feild is required.", this->ui->lineName);
            return;
        }
        if (this->ui->linePhone->text().isEmpty())
        {
            setErrorStyle(" ! This feild is required.", this->ui->linePhone);
            return;
        }
        else if (this->ui->linePhone->text().length() < 12)
        {
            setErrorStyle(" ! Too short for a phone number.", this->ui->linePhone);
            return;
        }
    }
    if (this->ui->comboTypes->currentIndex() == 2) // for organization account
    {
        if (this->ui->lineWork->text().isEmpty())
        {
            setErrorStyle(" ! This feild is required.", this->ui->lineWork);
            return;
        }
        else if (this->ui->lineWork->text().length() < 5)
        {
            setErrorStyle(" ! Too short for a username.", this->ui->lineWork);
            return;
        }
    }
    try
    {
        User* currentUser = new User();
        if (this->ui->comboTypes->currentIndex() == 0)
            currentUser = this->twitterak->signup(this->ui->comboTypes->currentIndex(), this->ui->lineUsername->text().toStdString(),
                                    this->ui->linePassword->text().toStdString(), this->ui->lineName->text().toStdString(),
                                    this->ui->linePhone->text().toStdString(), this->ui->lineWork->text().toStdString(),
                                    this->ui->lineCountry->text().toStdString(), this->ui->lineLink->text().toStdString(),
                                    this->bioTextEdit->toPlainText().toStdString(), ui->comboHeader->currentText().toStdString(),
                                    this->ui->dateEdit->date().day(), this->ui->dateEdit->date().month(), this->ui->dateEdit->date().year());
        else if (this->ui->comboTypes->currentIndex() == 1)
            currentUser = this->twitterak->signup(this->ui->comboTypes->currentIndex(), this->ui->lineUsername->text().toStdString(),
                                    this->ui->linePassword->text().toStdString());
        else if (this->ui->comboTypes->currentIndex() == 2)
            currentUser = this->twitterak->signup(this->ui->comboTypes->currentIndex(), this->ui->lineUsername->text().toStdString(),
                                    this->ui->linePassword->text().toStdString(), this->ui->lineName->text().toStdString(),
                                    this->ui->linePhone->text().toStdString(), this->ui->lineWork->text().toStdString(),
                                    this->ui->lineCountry->text().toStdString(), this->ui->lineLink->text().toStdString(),
                                    this->bioTextEdit->toPlainText().toStdString(), ui->comboHeader->currentText().toStdString());
        qDebug() << "here in before fuck";
        if (!profile.isEmpty())
        {
            if(QFile::exists("../Files/Profiles/" + QString::number(currentUser->getID())))
            {
                QFile::remove("../Files/Profiles/" + QString::number(currentUser->getID()));
            }
            QFile::copy(profile, "../Files/Profiles/" + QString::number(currentUser->getID()));
        }
        LoggedInWindow* loggedInWindow = new LoggedInWindow(nullptr, currentUser);
        loggedInWindow->show();
        this->close();
        this->mainWindow->close();
        QMessageBox::information(this, "Well Done!", " * Registration was successful.");
    }
    catch (std::invalid_argument & ex)
    {
        if ((std::string(ex.what()) == std::string(" ! The username is taken.")) ||
            (std::string(ex.what()) == std::string(" ! This is a reserved keyword.")))
        {
            setErrorStyle(ex.what(), this->ui->lineUsername);
        }
        else if ((std::string(ex.what()) == std::string(" ! The account is not organization.")) ||
                 (std::string(ex.what()) == std::string(" ! The account is not personal.")) ||
                 (std::string(ex.what()) == std::string(" ! User not found.")))
        {
            setErrorStyle(ex.what(), this->ui->lineWork);
        }
        else if (std::string(ex.what()) == std::string(" ! This phone number is used."))
        {
            setErrorStyle(ex.what(), this->ui->linePhone);
        }
        else if (std::string(ex.what()) == std::string(" ! Too weak."))
        {
            setErrorStyle(ex.what(), this->ui->linePassword, this->ui->passwordBox);
        }
        else if (std::string(ex.what()) == std::string(" ! This is a reserved name."))
        {
            setErrorStyle(ex.what(), this->ui->lineName);
        }
    }
}

void Signup::changeStyleFilled(const QString &)
{
    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(sender());
    if (lineEdit) {
        lineEdit->setStyleSheet("QLineEdit:disabled{border: 2px solid gray; border-radius: 20px; font-color: gray;}"
                                "QLineEdit:enabled{border: 2px solid black; border-radius: 20px; font-color: black;}");
    }
    if (lineEdit == ui->linePassword)
    {
        this->ui->linePassword->setStyleSheet("border: none; border-radius: 0px; background-color: rgba(255, 255, 255, 0); color: rgb(0, 0, 0);");
        this->ui->passwordBox->setStyleSheet("border: 2px solid black; border-radius: 20px;");
    }
    lineEdit->setPlaceholderText(NULL);
    if (lineEdit == ui->linePhone)
    {
        this->ui->linePhone->setPlaceholderText(" (must start with 989, 12 characters)");
    }
}
