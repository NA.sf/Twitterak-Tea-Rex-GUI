#include "mytextedit.hpp"

#include <QTextEdit>
#include <QGuiApplication>
#include <QScreen>

MyTextEdit::MyTextEdit(QWidget *parent, int limitation, bool isTweet) :
    QTextEdit(parent),
    MAX_LENGTH(limitation),
    isTweet(isTweet) {}

void MyTextEdit::keyPressEvent(QKeyEvent *e) {
    if (((this->MAX_LENGTH > 0 && toPlainText().length() >= this->MAX_LENGTH && e->text().length() > 0 && e->key() != Qt::Key_Backspace)) || // checks if it is more than limitation
        ((!isTweet) && ((e->key() == Qt::Key_Return) || (e->key() == Qt::Key_Enter))) ||
        ((isTweet) && (e->text() == '~')))
    {
        e->ignore();
    }
    else
    {
        QTextEdit::keyPressEvent(e);
    }
}

void MyTextEdit::insertFromMimeData(const QMimeData *source)
{
    if (source->hasText() && this->MAX_LENGTH > 0)
    {
        QString str = source->text();
        if (str.length() > (this->MAX_LENGTH - this->toPlainText().length()))
        {
            str = str.left(this->MAX_LENGTH - this->toPlainText().length());
        }
        QMimeData* newSource = new QMimeData();
        newSource->setText(str);
        QTextEdit::insertFromMimeData(newSource);
        delete newSource;
    }
    else
    {
        QTextEdit::insertFromMimeData(source);
    }
}

void MyTextEdit::setLimitation(int limitedChar)
{
    this->MAX_LENGTH = limitedChar;
}

int MyTextEdit::getLimitation()
{
    return this->MAX_LENGTH;
}

void MyTextEdit::fixText() //
{
    QStringList lines = this->toPlainText().split("\n");
    for(int i = 0; i < lines.size(); i++)
    {
        QString& str = lines[i];
        if(str.trimmed().isEmpty() || str.isEmpty())
        {
            lines.removeAt(i);
            --i;
        }
        else str = str.trimmed();
    }
    this->setText(lines.join('\n'));
}


void MyTextEdit::mouseReleaseEvent(QMouseEvent *event)
{
    QTextEdit::mouseReleaseEvent(event); // call the base class implementation
    QString word = textCursor().selectedText();
    if (word.startsWith("m")) {
        this->setPlainText("kjhjhkh");
    }
}
void MyTextEdit::adjustHeight() {
    //    this->setWordWrapMode(QTextOption::WrapAnywhere);
    //    int contentHeight = this->height();
    //    this->setPlainText(QString::number(this->document()->size().height()));
    //    this->setFixedHeight( contentHeight);
    QScreen *screen = QGuiApplication::primaryScreen();
    qreal dpi = screen->logicalDotsPerInch();
    qreal docHeight = this->document()->documentLayout()->documentSize().height();

    // Convert points to pixels
    int pixels = docHeight * (dpi / 72.0);

    // Set the QTextEdit's height
    this->setFixedHeight(pixels);
}
//void paintEvent(QPaintEvent *event)
//{
//    // Call base class's paintEvent to ensure normal painting
////    QPlainTextEdit::paintEvent(event);

//    // Calculate the height based on the content
////    QTextDocument* doc = this->document();
////    qreal height = doc->documentLayout()->documentSize().height();

////    // Determine any additional padding you want to add to the height
////    // based on your requirements
////    int padding = 5;
////    height += padding;

////    // Set the calculated height to the widget
////    this->setFixedHeight(height);
//}
