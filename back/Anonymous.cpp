#include <string>
#include <stdexcept>
#include "User.hpp"
#include "Anonymous.hpp"

void Anonymous::setName(const std::string& input)
{
    if (!input.empty()) throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setBiography(const std::string& input)
{
    if (!input.empty()) throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setCountry(const std::string& input)
{
    if (!input.empty()) throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setLink(const std::string& input)
{
    if (!input.empty()) throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setPhoneNumber(const std::string& input)
{
    if (!input.empty()) throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setHeader(const std::string& input)
{
    if (input != "White") throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}
void Anonymous::setWork(User*)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setBirthDay(int)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setBirthMonth(int)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

void Anonymous::setBirthYear(int)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to modify this.");
}

std::string Anonymous::getBirthDate() const
{
    throw std::invalid_argument (" ! User is Anonymous.");
}

void Anonymous::setFilePhone(const std::string &)
{
    throw std::invalid_argument("! User doesn't have phone number.");
}

void Anonymous::deleteTweet(int)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to do this.");
}

void Anonymous::changeTweet(int, std::string&)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to do this.");
}

std::vector<Tweet *> Anonymous::getVecTweet()
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to do this.");
}

void Anonymous::setVec(const std::string&)
{
    throw std::invalid_argument(" ! Access Denied: You do not have permission to do this.");
}

//User* Anonymous::getWork()
//{
////    throw std::invalid_argument("\tnothing14");
//    return nullptr;
//}

void Anonymous::saveProfile(std::ofstream& out, std::string& stringOrganization)
{
    out << '\n' << "A.";
    User::saveProfile(out, stringOrganization);
    User::savePreviousPasswords(out);
}

void Anonymous::loadProfile(std::ifstream& in)
{
    User::loadProfile(in);
    User::loadPreviousPasswords(in);
}

void Anonymous::deleteTweets() {} // overrides it cause they have no tweets

void Anonymous::setFollower(User*)
{
    throw std::invalid_argument(" ! Access Denied: You can't follow this accoumt.");
}
