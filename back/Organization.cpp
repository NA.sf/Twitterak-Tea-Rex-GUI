#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <ctime>
#include <fstream>

//#include "sha256.h"
#include "User.hpp"
#include "Organization.hpp"
#include "Personal.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"
#include "Mention.hpp"
#include "Twitterak.hpp"

void Organization::setBiography(const std::string& input)
{
    if (input.length() < 1101) User::setBiography(input);
    else throw std::invalid_argument(" ! Input Too Long: The text you entered is too long. Please limit your input to 1100 characters.");
}

//void Organization::setBirthDay(int dd)
//{
//    throw std::invalid_argument("Nothing");
//}

//void Organization::setBirthMonth(int mm)
//{
//    throw std::invalid_argument("Nothing");
//}

//void Organization::setBirthYear(int yy)
//{
//    throw std::invalid_argument("Nothing");
//}

std::string Organization::getBirthDate() const 
{
    throw std::invalid_argument(" Organization users don't have birthdate!");
}

void Organization::setWork(User* work)
{
    if (work != nullptr)
    {
        try
        {
            work->getBirthDate();
            User::setWork(work);
        }
        catch (const std::invalid_argument&)
        {
            throw std::invalid_argument(" ! There is no personal account with this username. Enter a valid username.");
        }
    }
    else
    {
        throw std::invalid_argument(" ! There is no personal account with this username. Enter a valid username.");
    }
}

void Organization::saveProfile(std::ofstream& out, std::string& stringOrganization)
{
    out << '\n' << "O.";
    User::saveProfile(out, stringOrganization);
    out << this->getName() << '\n' << this->getPhoneNumber() << '\n' << this->getCountry() << '\n'
        << this->getWork()->getID() << '\n' << this->getLink() << '\n' << this->getHeader() << '\n'
        << this->getBiography() << '\n';
    User::savePreviousPasswords(out);
}

void Organization::loadProfile(std::ifstream& in)
{
    User::loadProfile(in);
    std::string input;
    int workID;
    in.ignore(2);
    getline(in, input);
    this->setName(input);
    getline(in, input);
    this->setFilePhone(input);
    getline(in, input);
    this->setCountry(input);
    in >> workID;
    in.ignore();
    this->setWork(Twitterak::getUser("", workID));
    getline(in, input);
    this->setLink(input);
    getline(in, input);
    this->setHeader(input);
    getline(in, input);
    this->setBiography(input);
    User::loadPreviousPasswords(in);
}
