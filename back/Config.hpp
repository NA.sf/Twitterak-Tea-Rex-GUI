#include <QLineEdit>
#include <QGroupBox>
#include <QString>

void setErrorStyle(const QString&, QLineEdit* line = nullptr, QGroupBox* group = nullptr);
