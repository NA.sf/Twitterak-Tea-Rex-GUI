// Date class definition; Member functions defined in Date.cpp

#ifndef DATE_HPP
#define DATE_HPP

#include <iostream>

class Date
{
public:
    Date();
    Date(int, int, int);
    void setDay(int);
    void setMonth(int);
    void setYear(int);

    int getDay() const;
    int getMonth() const;
    int getYear() const;
    std::string printDate() const;
    
private:
    int day, month, year;
};

#endif
