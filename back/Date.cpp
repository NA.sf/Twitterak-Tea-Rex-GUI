// Date class member-function definitions.

#include <iostream>
#include <ctime>
#include <stdexcept>
#include <sstream>

#include "Date.hpp"

Date::Date() : day{0}, month{0}, year{0}
{
}

Date::Date(int dd, int mm, int yy) : day{dd}, month{mm}, year{yy}
{
}

void Date::setDay(int inputDay)
{
   this->day = inputDay;
}

void Date::setMonth(int inputMonth)
{
   this->month = inputMonth;
}

void Date::setYear(int inputYear)
{
   this->year = inputYear;
}

int Date::getDay() const
{
   return this->day;
}

int Date::getMonth() const
{
   return this->month;
}

int Date::getYear() const
{
   return this->year;
}

std::string Date::printDate() const
{
   std::ostringstream output;
   switch (this->month)
   {
      case 1:
         output << "Jan ";
         break;
      case 2:
         output << "Feb ";
         break;
      case 3:
         output << "Mar ";
         break;
      case 4:
         output << "Apr ";
         break;
      case 5:
         output << "May ";
         break;
      case 6:
         output << "Jun ";
         break;
      case 7:
         output << "Jul ";
         break;
      case 8:
         output << "Aug ";
         break;
      case 9:
         output << "Sep ";
         break;
      case 10:
         output << "Oct ";
         break;
      case 11:
         output << "Nov ";
         break;
      case 12:
         output << "Dec ";
         break;
   }
   output << this->day << ", " << this->year;
   return output.str();
}
