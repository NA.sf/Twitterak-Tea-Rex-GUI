#include "Config.hpp"

void setErrorStyle(const QString& error, QLineEdit* line, QGroupBox* group)
{
    line->clear();
    if (group == nullptr)
    {
        line->setStyleSheet("QLineEdit { color: rgba(255, 0, 0, 127); border: 2px solid red; } QLineEdit::focus { color: black; }");
    }
    else
    {
        group->setStyleSheet("border: 2px solid red;");
        line->setStyleSheet("QLineEdit { color: rgba(255, 0, 0, 127); border: none; background-color: rgba(255, 255, 255, 0);} QLineEdit::focus { color: black; }");
    }
    line->setPlaceholderText(error);
}
