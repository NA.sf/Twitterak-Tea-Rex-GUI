#include <iostream>
#include <string>

#include "User.hpp"
#include "Mention.hpp"

Mention::Mention(std::string text, User * user) :
    mentionText(text), // set the mention text
    mentioner(user) // set a pointer to the user that has mentioned
{
}

//void Mention::setMention(std::string & text,User * user)
//{
//    this->mentionText = text;
//    this->mentioner = user;
//}

std::string Mention::getMentionText()
{
    return this->mentionText;
}

User* Mention::getMentioner()
{
    return this->mentioner;
}

void Mention::setLike(User * user)
{
    this->likes.push_back(user);
//    for (User * liker : likes)
//    {
//        if (liker == user)
//        {
////            this->likes->re
//        }
//    }
}

void Mention::saveLikers(std::ofstream& out)
{
    for (size_t i{0}; i < this->likes.size(); ++i) // write IDs of users that have liked the mention
    {
        try
        {
            out << this->likes[i]->getID() << '-';
        }
        catch (std::bad_alloc&)
        {
            this->likes.erase(this->likes.begin() + i);
            --i;
        }
    }
}
