#ifndef PERSONAL_HPP
#define PERSONAL_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Tweet.hpp"
#include "Date.hpp"
#include "Mention.hpp"
#include "User.hpp"

class Personal:public User
{
using User::User;
public:
    void setWork(User*) override;
    void setBiography(const std::string&) override;
    void saveProfile(std::ofstream&, std::string&) override;
    void loadProfile(std::ifstream&) override;

//     Personal(int, int, int);
//     void setUsername(std::string, const std::vector<User *> &) override;
//     void setName(std::string)override;
//     void setBiography(std::string)override;
//     void setCountry(std::string)override;
//     void setLink(std::string)override;
//     void setPhoneNumber(std::string, const std::vector<User *> &)override;
//     void setPassword(const std::string &)override;
//     void setHeader(std::string)override;
//     void setBirthDay(int)override;
//     void setBirthMonth(int)override;
//     void setBirthYear(int)override;

//     std::string getUsername() const override;
//     std::string getName() const override;
//     std::string getBiography() const override;
//     std::string getCountry() const override;
//     std::string getLink() const override;
//     std::string getPhoneNumber() const override;
//     std::string getPassword() const override;
//     std::string getHeader() const override;
//     std::string getJoinDate() const override;
//     std::string getBirthDate() const override;
//     std::string showColor() const override; // for showing header's color

//     bool isOld() const override; // For edit tweet
//     std::vector<Tweet *> getVecTweet()override; // User's twwets
//     void deleteTweet(int)override;
//     void changeTweet(int, std::string &)override;
//     void setVec(std::string &)override; // Adds new twee
// private:
    // std::string name, username, biography, country, link, phoneNumber, password, header;
    // Date birthDate;
    // std::vector <Tweet *> vecTweet;
    // const Date joinDate;
};

#endif
