// User class member-function definitions.

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <ctime>

//#include "sha256.h"
#include "User.hpp"
#include "Personal.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"
#include "Mention.hpp"



void Personal::setWork(User* work)
{
    if (work == nullptr)
    {
        User::setWork(work);
        return;
    }
    else
    {
        try
        {
            work->getBirthDate();
            throw std::invalid_argument(" ! The account is not organization.");
        }
        catch(const std::invalid_argument& ex)
        {
            if (ex.what() == std::string(" Organization users don't have birthdate!"))
            {
                User::setWork(work);
            }
            else
            {
                throw std::invalid_argument(" ! The account is not organization.");
            }
        }
    }
}

void Personal::saveProfile(std::ofstream& out, std::string& stringOrganization)
{
    out << '\n' << "P.";
    User::saveProfile(out, stringOrganization);
    out << this->getName() << '\n' << this->getPhoneNumber() << '\n' << this->getCountry() << '\n'
        << this->getLink() << '\n' << this->getBirthDate() << '\n' << this->getHeader() << '\n' << this->getBiography() << '\n';
    User::savePreviousPasswords(out);
    if (this->getWork() != nullptr) stringOrganization += std::to_string(this->getID()) + ":" + std::to_string(this->getWork()->getID()) + "-";
}

void Personal::loadProfile(std::ifstream& in)
{
    User::loadProfile(in);
    std::string input;
    getline(in, input);
    this->setName(input);
    getline(in, input);
    this->setFilePhone(input);
    getline(in, input);
    this->setCountry(input);
    getline(in, input);
    this->setLink(input);
    getline(in, input);
    this->setBirthDate(input);
    getline(in, input);
    this->setHeader(input);
    getline(in, input);
    this->setBiography(input);
    User::loadPreviousPasswords(in);
}

void Personal::setBiography(const std::string& input)
{
    if (input.length() < 161) User::setBiography(input);
    else throw std::invalid_argument(" ! Input Too Long: The text you entered is too long. Please limit your input to 160 characters.");
}

// Personal::Personal(int joinYear, int joinMonth, int joinDay) : User::User(joinYear, joinMonth, joinDay)
// {
    
// }

// void Personal::setUsername(std::string input, const std::vector<User *>& users)
// {
//     User::setUsername(input, users);
//     // if (input.size() < 5)
//     // {
//     //     throw std::invalid_argument("\t! The username is too short. (at least 5 characters)");
//     // }
//     // if (((input[0] - '0') > -1) && ((input[0] - '0') < 10))
//     // {
//     //     throw std::invalid_argument("\t! The first character can't be a number.");
//     // }
//     // for (int i = 0; i < input.size(); i++)
//     // {
//     //     if (!(((int(input[i]) < 58) && (int(input[i]) > 46)) || ((int(input[i]) < 123) && (int(input[i]) > 96))))
//     //     {
//     //         throw std::invalid_argument("\t! Invalid character(s).");
//     //     }
//     // }
//     // std::string keyWords = input;
//     // std::transform(keyWords.begin(), keyWords.end(), keyWords.begin(), ::tolower);
//     // if (keyWords == "login" || keyWords == "signup" || keyWords == "username" || keyWords == "biography" || keyWords == "country" || keyWords == "phonenumber" ||
//     //     keyWords == "password" || keyWords =="header" || keyWords == "birthdate" || keyWords == "delete" || keyWords == "account" || keyWords == "profile" || keyWords == "tweet" || keyWords == "dislike" || keyWords == "logout")
//     // {
//     //     throw std::invalid_argument("\t! This is a reserved keyword. You can not choose it as your username.");
//     // }
//     // for (User * duplicateUser : users)
//     // {
//     //     if (duplicateUser->getUsername() == input)
//     //     {
//     //         throw std::invalid_argument("\t! The username is taken.");
//     //     }
//     // }
//     // this->username = input;
// }

// void Personal::setName(std::string input)
// {
//     User::setName(input);
//     // this->name = input;
// }

// void Personal::setBiography(std::string input)
// {
//     User::setBiography(input);
//     // if (input.size() > 160)
//     // {
//     //     throw std::invalid_argument("\t! The biography is too long. (Up to 160 characters)");
//     // }
//     // this->biography = input;
// }

// void Personal::setCountry(std::string input)
// {
//     User::setCountry(input);
//     // this->country = input;
// }

// void Personal::setLink(std::string input)
// {
//     User::setLink(input);
//     // if ((input.size() > 0) && (input.substr(0, 8) != "https://"))
//     // {
//     //     this->link = "https://" + input;
//     // }
//     // else
//     // {
//     //     this->link = input;
//     // }
// }

// void Personal::setPhoneNumber(std::string input, const std::vector<User *>& users)
// {
//     User::setPhoneNumber(input, users);
//     // for(int i = 0; i < input.size(); i++)
//     // {
//     //     if(((input[i] - '0') < 0) || ((input[i] - '0') > 9)) 
//     //     {
//     //         throw std::invalid_argument("\t! The phone number isn't valid.");
//     //     }
//     // }
//     // if ((input.size() == 10) && (input[0] == '9'))
//     // {
//     //     input = "98" + input;
//     // }
//     // else if ((input.size() == 11) && (input.substr(0, 2) == "09"))
//     // {
//     //     input = "98" +  input.substr(1, 10);
//     // }
//     // else if (!((input.size() == 12) && (input.substr(0, 3) == "989")))
//     // {
//     //     throw std::invalid_argument("\t! The phone number isn't valid.");
//     // }
//     // for (User * duplicateUser : users)
//     // {
//     //     if (duplicateUser->getPhoneNumber() == input)
//     //     {
//     //         throw std::invalid_argument("\t! This phone number is used.");
//     //     }
//     // }
//     // this->phoneNumber = input;
// }

// void Personal::setPassword(const std::string& input)
// {
//     User::setPassword(input);
//     // for (int i = 0; i < input.size(); i++)
//     // {
//     //     if (input[i] == ' ')
//     //     {
//     //         throw std::invalid_argument("\t! Invalid character(s).");
//     //     }
//     // }
//     // SHA256 sh;
//     // this->password = sh(input);
// }

// void Personal::setHeader(std::string input)
// {
//     User::setHeader(input);
//     // std::transform(input.begin(), input.end(), input.begin(), ::tolower);
//     // if (input == "white" || input == "red" || input == "orange" || input == "yellow" || input == "pink" ||
//     //     input == "green" || input == "blue" || input == "purple" || input == "black")
//     // {
//     //     this->header = input;
//     // }
//     // else if (input == "")
//     // {
//     //     this->header = "white";
//     // }
//     // else
//     // {
//     //     throw std::invalid_argument("\t! The header is not valid!");
//     // }
// }

// void Personal::setBirthDay(int dd)
// {
//     User::setBirthDay(dd);
//     // birthDate.setDay(dd);
// }

// void Personal::setBirthMonth(int mm)
// {
//     User::setBirthMonth(mm);
//     // birthDate.setMonth(mm);
// }

// void Personal::setBirthYear(int yy)
// {
//     User::setBirthYear(yy);
//     // birthDate.setYear(yy);
// }

// std::string Personal::getUsername() const 
// {
//     return User::getUsername();
//     // return this->username;
// }

// std::string Personal::getName() const 
// {
//     return User::getName();
//     // return this->name;
// }

// std::string Personal::getBiography() const 
// {
//     return User::getBiography();
//     // return this->biography;
// }

// std::string Personal::getCountry() const 
// {
//     return User::getCountry();
//     // return this->country;
// }

// std::string Personal::getLink() const 
// {
//     return User::getLink();
//     // return this->link;
// }

// std::string Personal::getPhoneNumber() const 
// {
//     return User::getPhoneNumber();
//     // return this->phoneNumber;
// }

// std::string Personal::getPassword() const 
// {
//     return User::getPassword();
//     // return this->password;
// }

// std::string Personal::getHeader() const 
// {
//     return User::getHeader();
//     // return this->header;
// }

// std::string Personal::getBirthDate() const 
// {
//     return User::getBirthDate()();
//     // return this->birthDate.printDate();
// }

// std::string Personal::showColor() const 
// {
//     return User::showColor();
//     // std::string Header = this->header;
//     // if (Header == "red")
//     // {
//     //     return RED;
//     // }
//     // else if (Header == "orange")
//     // {
//     //     return ORANGE;
//     // }
//     // else if (Header == "yellow")
//     // {
//     //     return YELLOW;
//     // }
//     // else if (Header == "pink")
//     // {
//     //     return PINK;
//     // }
//     // else if (Header == "green")
//     // {
//     //     return GREEN;
//     // }
//     // else if (Header == "blue")
//     // {
//     //     return BLUE;
//     // }
//     // else if (Header == "purple")
//     // {
//     //     return PURPLE;
//     // }
//     // else if (Header == "black")
//     // {
//     //     return BLACK;
//     // }
//     // return "";
// }

// std::string Personal::getJoinDate() const 
// {
//     return User::getJoinDate();
//     // return this->joinDate.printDate();
// }

// bool Personal::isOld() const 
// {
//     return User::isOld();
//     // return this->birthDate.isOldEnough();
// }

// void Personal::deleteTweet(int num) 
// {
//     User::deleteTweet(num);
//     // this->vecTweet[num] = nullptr;
// }

// void Personal::changeTweet(int num, std::string& str)
// {
//     User::deleteTweet(num);
//     // vecTweet[num - 1]->setText(str); 
// }

// std::vector<Tweet *> Personal::getVecTweet() 
// {
//     return this->vecTweet;
// }

// void Personal::setVec(std::string& st)
// {
//     Tweet newTweet;
//     newTweet.setText(st);
//     this->vecTweet.push_back(new Tweet(newTweet));
// }
