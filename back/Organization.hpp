#ifndef ORGANIZATION_HPP
#define ORGANIZATION_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Tweet.hpp"
#include "Date.hpp"
#include "Mention.hpp"
#include "Personal.hpp"

class Organization :public User
{
using User::User;
public:
    Organization(int, int, int);
    void setBiography(const std::string&) override;
//    void setWork(User*, const std::vector<User *> &)override;
//    void setBirthDay(int) override;
//    void setBirthMonth(int) override;
//    void setBirthYear(int) override;

    std::string getBirthDate() const override;

    void setWork(User*) override;
    void saveProfile(std::ofstream&, std::string&) override;
    void loadProfile(std::ifstream&) override;
};

#endif
