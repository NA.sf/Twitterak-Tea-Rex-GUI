// Tweet class member-function definitions.

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <QDebug>
#include "Date.hpp"
#include "Tweet.hpp"
#include "User.hpp"
#include "Config.hpp"
#include "Mention.hpp"
#include "Hashtag.hpp"

Tweet::Tweet()
{
}

void Tweet::setText(const std::string& input )
{
    this->text = input;
}

std::string Tweet::getText()
{
    return this->text;
}

void Tweet::setLike(User * user)
{

    qDebug() << "user: "  << user;
    qDebug() << "this: "  << this;
    this->likes.push_back(user);
    qDebug()  << "in set like2";
}

int Tweet::getSize()
{
    return this->likes.size();
}

void Tweet::seDislike(User * user)
{
    int pos = 0;
    for (User * liker :likes)
    {
        if (liker == user)
        {
            likes.erase(likes.begin() + pos);
            return;
        }
        pos++;
    }
//    std::cout << RED << "\t! You haven't liked this tweet.\n" << RESET;
}

void Tweet::showLike()
{
    if (likes.size() == 0) {/*std::cout << RED << "\t! Nobody has liked this tweet.\n" << RESET;*/}
    else
    {
        for(int liker = 0; liker < likes.size(); liker++)
        {
            try
            {
                std::cout << "\t"<< this->likes.at(liker)->getUsername() << "\n";
            }
            catch (std::bad_alloc&) {}
        }
    }
}

Mention* Tweet::setMention(std::string& mentionText, User* mentioner) // return Mention* for when loading from file
{                                                                     // set like for mention after setting it
    this->mentionTweet.push_back(new Mention(mentionText, mentioner));
    return this->mentionTweet[this->mentionTweet.size() - 1];         // return the address of the last mention that was just set
}

void Tweet::loadHashtags(User* tweeter, Tweet* tweet)
{
//    qDebug() << "here in loadHashtags() for " << tweet->getText();
    std::string tweetText{this->text}; // hold the text of tweet
    std::string hashtagText; // hold the text of found hashtag
    int hashtagPos; // hold the position of first # in text of tweet
    int spacePos{-1}; // hold the position of first space after # in text of tweet
    while (tweetText.find("#", spacePos + 1) != std::string::npos) // search for hashtag in the text of tweet
    {
//        qDebug() << "here in while for ";
        hashtagPos = tweetText.find("#", spacePos + 1);
        if (tweetText.find(" ", hashtagPos + 1) != std::string::npos) spacePos = tweetText.find(" ", hashtagPos + 1);
        else spacePos = tweetText.size();
        hashtagText = tweetText.substr(hashtagPos, spacePos - hashtagPos);
        Hashtag* foundHashtag = new Hashtag();
        this->hashtags.push_back(foundHashtag->setHashtag(tweeter, tweet, hashtagText)); // set the hashtag in hashtag class and for tweet
    }
}

void Tweet::saveLikers(std::ofstream& out)
{
    for (size_t i{0}; i < this->likes.size(); ++i) // write IDs of users that have liked the tweet
    {
        try
        {
            out << this->likes[i]->getID() << '-';
        }
        catch(std::bad_alloc&)
        {
            this->likes.erase(this->likes.begin() + i);
            --i;
        }
    }
    out << '\n';
    for (size_t i{0}; i < this->mentionTweet.size(); ++i) // write the mention
    {
        out << this->mentionTweet[i]->getMentioner()->getID() << ':' // write the user who has mentioned by their ID
            << this->mentionTweet[i]->getMentionText() << '~'; // write the mention's text
        this->mentionTweet[i]->saveLikers(out); // write likes of the mention in file
        out << '\n';
    }
}

void Tweet::deleteTweet()
{
    this->deleteHashtags();
    this->deleteMentions();
}

void Tweet::deleteMentions()
{
    for (size_t i{0}; i < this->mentionTweet.size(); ++i)
    {
        delete this->mentionTweet[i];
    }
    this->mentionTweet.clear();
}


void Tweet::deleteHashtags()
{
    for (size_t i{0}; i < this->hashtags.size(); ++i)
    {
        this->hashtags[i]->deleteTweet(this);
    }
    this->hashtags.clear();
}

bool Tweet::checkLike(User* user)
{
    for (User * liker : likes)
    {
        if (liker == user)
        {
            qDebug() << "true";
            return true;
        }
    }
    qDebug() << "false";
    return false;
}
void Tweet::setMention(Mention* mention)
{
    mentionTweet.push_back(mention);
}
