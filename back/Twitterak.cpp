// Twitterak class member-function definitions.

#include <iostream>
#include <string>
#include <unistd.h>
#include <iomanip>
#include <algorithm>
#include <sstream>

#include <fstream>
#include <filesystem>
#include <QDebug>
#include <QString>


#include "hash-library/sha256.h"
#include "Twitterak.hpp"
#include "User.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"

#include "Anonymous.hpp"
#include "Personal.hpp"

#include "Organization.hpp"

std::vector<User*> Twitterak::users;

User* Twitterak::getUser(std::string userName, int userID)
{
    if (userName == "")
    {
        for (User* founder : users)
        {
            if (founder->getID() == userID)
            {
                return founder;
            }
        }
    }
    else if (userID == 0)
    {
        for (User* founder : users)
        {
            if (founder->getUsername() == userName)
            {
                return founder;
            }
        }
    }
    return nullptr;
}

User* Twitterak::checkLogin(const std::string& userName, const std::string& passWord)
{
    SHA256 sh;
    User* theUser = new User();
    for (User * foundUser : users)
    {
        if (foundUser->getUsername() == userName)
        {
            if(foundUser->getPassword() == sh(passWord))
            {
                theUser = foundUser;
                return theUser;
            }
            else
            {
                throw std::invalid_argument(" ! Password is not correct.");
            }
        }
    }
    throw std::invalid_argument(" ! User doesn't exist.");
}

void Twitterak::editProfile(User* currentUser, const std::string& userName, const std::string& passWord, const std::string& name,
                        const std::string& phoneNumber, const std::string& work, const std::string& country,
                        const std::string& link, const std::string& biography, const std::string& header)
{
    currentUser->setUsername(userName);
    if (!passWord.empty()) currentUser->setPassword(passWord);
    currentUser->setName(name);
    currentUser->setPhoneNumber(phoneNumber);
    currentUser->setWork(this->getUser(work));
    currentUser->setCountry(country);
    currentUser->setLink(link);
    currentUser->setBiography(biography);
    currentUser->setHeader(header);
}

User* Twitterak::signup(int index, const std::string& userName, const std::string& passWord, const std::string& name,
                                  const std::string& phoneNumber, const std::string& work, const std::string& country,
                                  const std::string& link, const std::string& biography, const std::string& header,
                                  const int& day, const int& month, const int& year)
{
    User *theUser;
    if (index == 0)
    {
        Personal * personalPtr = new Personal();
        theUser = dynamic_cast<User*>(personalPtr);
    }
    else if (index == 1)
    {
        Anonymous *anonymousPtr = new Anonymous();
        theUser = dynamic_cast<User*>(anonymousPtr);
    }
    else if (index == 2)
    {
        Organization *organizationPtr = new Organization();
        theUser = dynamic_cast<User*>(organizationPtr);
    }
    for (User * duplicateUser : users)
    {
        if (duplicateUser->getUsername() == userName)
        {
            throw std::invalid_argument(" ! The username is taken.");
        }
    }
    theUser->setUsername(userName);
    theUser->setPassword(passWord);
    if (index != 1)
    {
        theUser->setName(name);
        for (User * duplicateUser : users)
        {
            if (duplicateUser->getPhoneNumber() == phoneNumber)
            {
                throw std::invalid_argument(" ! This phone number is used.");
            }
        }
        theUser->setPhoneNumber(phoneNumber);
        theUser->setBiography(biography);
        theUser->setCountry(country);
        theUser->setLink(link);
        theUser->setHeader(header);
        theUser->setBirthYear(year);
        theUser->setBirthMonth(month);
        theUser->setBirthDay(day);
        if (work != "")
        {
            if (this->getUser(work) != nullptr)
            {
                try
                {
                    theUser->setWork(this->getUser(work));
                }
                catch(std::invalid_argument & ex)
                {
                    if (index == 2)
                    {
                        throw std::invalid_argument(" ! The account is not personal.");
                    }
                    if (index == 0)
                    {
                        throw std::invalid_argument(" ! The account is not organization.");
                    }
                }
            }
            else throw std::invalid_argument(" ! User not found.");
        }
    }
    if (this->usersSize() > 0) theUser->setID(users[this->usersSize() - 1]->getID() + 1);
    else theUser->setID(1);
    users.push_back(theUser);
    return theUser;
}


void Twitterak::deleteAccount(User* currentUser)
{
    int pos{0};
    for (User * foundUser : this->users)
    {
        if (foundUser == currentUser)
        {
            qDebug() << "here in delete account!";
            foundUser->deleteTweets();
            foundUser->deleteFollows();
            delete users[pos];
            users.erase(users.begin() + pos);
            return;
        }
        pos++;
    }

}

void Twitterak::saveProfiles()
{
    qDebug() << "here in saveprofiles!";
    if (!(std::filesystem::exists("../Files")))
    {
        std::filesystem::create_directory("../Files");
    }
    std::ofstream out("../Files/Profiles.txt", std::ios::out);
    if (out)
    {
        std::string stringOrganization{""}; // for personal accounts that have organization in their profile to be added at the end of line
        out << users.size() << '\n'; // writes number of users at first of file
        for (User * theUser : users)
        {
            theUser->saveProfile(out, stringOrganization);
        }
        out << stringOrganization;
        out.close();
    }
}

void Twitterak::loadProfiles()
{
    qDebug() << "Here in load files\n";
    int usersNumber; // number of all students
    std::ifstream in("../Files/Profiles.txt", std::ios::in);
    qDebug() << "\nbefore check file\n";
    if(in)
    {
        qDebug() << "\nafter check file\n";
        char type;
        in >> usersNumber; // reads number of all users
        qDebug() << "\nafter load number\n";
        in.ignore(); // ignore lines to get to first user
        for (int index{0}; index < usersNumber; ++index)
        {
            in.ignore();
            qDebug() << "\nbefore load type\n";
            in >> type; // check the accounts type (A/P/O)
            qDebug() << "\nafter load type\n";
            if (type == 'A')
            {
                Anonymous * temp = new Anonymous();
                temp->loadProfile(in);
                User* userPtr = dynamic_cast<User*>(temp);
                users.push_back(userPtr);
            }
            else if (type == 'P')
            {
                Personal * temp = new Personal();
                temp->loadProfile(in);
                User* userPtr = dynamic_cast<User*>(temp);
                users.push_back(userPtr);
            }
            else if (type == 'O')
            {
                Organization * temp = new Organization();
                temp->loadProfile(in);
                User* userPtr = dynamic_cast<User*>(temp);
                users.push_back(userPtr);
            }
        }
        int employee, employer;
        while(in >> employee)
        {
            in.ignore();
            in >> employer;
            in.ignore();
            Twitterak::getUser("", employee)->setWork(Twitterak::getUser("", employer));
        }
        in.close();
    }
}

void Twitterak::saveTweets()
{
    if (!(std::filesystem::exists("../Files"))) // check if Files directory exists
    {
        std::filesystem::create_directory("../Files"); // if not make it
    }
    std::ofstream out("../Files/Tweets.txt", std::ios::out); // open the file to write in it
    if (out) // check if the file has been opened successfully
    {
        for (User* theUser : users) // write the user ID and their tweets
        {
            if (theUser->hasTweets()) // checks if user has any tweet
            {
                out << theUser->getID() << '*' << '\n';
                theUser->saveTweets(out);
            }
        }
        out.close();
    }
}

void Twitterak::loadTweets()
{
    std::ifstream in("../Files/Tweets.txt", std::ios::in);
    if(in)
    {
        char delimiter = '~'; // sign for ending a tweet's text
        std::string tweetOrMentionText; // text of tweet that is read from file
        std::stringstream stringToInt; // for setting number of likes
        int userOrTweetID, likerID, lastTweet{1};
        char sign; // sign for if it is a tweet or mention or userID
        std::string stringLikes; // for getting ID of
        std::string deletedTweet = " "; // for adding deleted tweets
        User * tweeter;
        while(in >> userOrTweetID)
        {
            in >> sign; // read the sign to know whether it should set user or tweet or mention
            if (sign == '*')
            {
                // set the user who tweeted
                for (User * foundUser : users)
                {
                    if (foundUser->getID() == userOrTweetID)
                    {
                        tweeter = foundUser; // find the user whose tweets will be in the rest of file till the next user
                        break;
                    }
                }
                in.ignore(); // go to next line
                continue;
            }
            getline(in, tweetOrMentionText, delimiter); // read the tweet/mention which text would be till '~'
            getline(in, stringLikes); // read the rest of line of file which is the ID of users who have liked the tweet/mention
            stringToInt = std::stringstream(stringLikes); // cast it to stringstream so it could get the liker's ID (int)
            if (sign == '.')
            {
                // add tweet
                while (lastTweet < userOrTweetID) // for adding deleted tweets to vector of tweets which are
                {                                 // the tweets that have not been written in the file
                    tweeter->setVec(deletedTweet);
                    ++lastTweet;
                }
                lastTweet = ++userOrTweetID;

                tweeter->setVec(tweetOrMentionText); // add tweet

                while (getline(stringToInt, stringLikes, '-')) // start setting likes for tweet (if there's any!)
                {
                    likerID = stoi(stringLikes);
                    for (User * foundUser : users) // search for user who has liked the tweet with their ID
                    {
                        if (foundUser->getID() == likerID)
                        {
                            tweeter->getLastTweet()->setLike(foundUser); // load likes for the tweet
                        }
                    }
                }
            }
            else if (sign == ':')
            {
                // add mention
                for (User * mentioner : users) // search for the user whose ID is the number before ':' and have write a mention
                {
                    if (mentioner->getID() == userOrTweetID)
                    {
                        Mention* mention = tweeter->getLastTweet()->setMention(tweetOrMentionText, mentioner); // add mention for last tweet that was loaded
                        while (getline(stringToInt, stringLikes, '-')) // start setting likes for mention (if there's any!)
                        {
                            likerID = stoi(stringLikes);
                            for (User * foundUser : users) // search for user who has liked the mention with their ID
                            {
                                if (foundUser->getID() == likerID)
                                {
                                    mention->setLike(foundUser); // load likes for the mention
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}

void Twitterak::saveFollows()
{
    if (!(std::filesystem::exists("../Files"))) // check if Files directory exists
    {
        std::filesystem::create_directory("../Files"); // if not make it
    }
    std::ofstream out("../Files/Follows.txt", std::ios::out); // open the file to write in it
    if (out)
    {
        for (User* theUser : users) // write the user ID and their followers and followings
        {
            if (theUser->hasFollowers() || theUser->hasFollowings()) // checks if user has any followers or followings
            {
                qDebug() << "here in for of save follow";
                out << theUser->getID() << '*'; // if user has any following or followers add the user's ID and '*' (sign to write followers)
                theUser->saveFollowers(out); // write the user's followers
                out << '|'; // sign to stop reading followers and strat reading followings
                theUser->saveFollowings(out); // write the user's followings
                out << '\n';
            }
        }
        out.close();
    }
}

void Twitterak::loadFollows()
{
    std::ifstream in("../Files/Follows.txt", std::ios::in);
    if(in)
    {
        int usersID; // ID of the user whose follower and followings are in the rest of line
        std::string stringFollow;
        std::stringstream stringToInt; // for setting followers and followings of users based on IDs
        User* theUser;
        while(in >> usersID)
        {
            for (User * foundUser : users)
            {
                if (foundUser->getID() == usersID)
                {
                    theUser = foundUser; // find the user whose followers and followings will be in the rest of line
                    break;
                }
            }
            in.ignore(); // ignore the '*' that is a sign to start reading the followers
            getline(in, stringFollow, '|'); // read the file till '|' which is a sign to start reading the followings
            stringToInt = std::stringstream(stringFollow);
            while (getline(stringToInt, stringFollow, '-'))
            {
                for (User * follower : users) // search for user who has followed the user based on ID
                {
                    if (follower->getID() == stoi(stringFollow))
                    {
                        theUser->setFollowerDirectly(follower); // add the follower to user's followers
                    }
                }
            }
            getline(in, stringFollow); // read the file till the end of line which is a sign to stop reading the followings
            stringToInt = std::stringstream(stringFollow);
            while (getline(stringToInt, stringFollow, '-'))
            {
                for (User * following : users) // search for user who has been followed by user based on ID
                {
                    if (following->getID() == stoi(stringFollow))
                    {
                        theUser->setFollowingDirectly(following); // add the followed account to user's followings
                    }
                }
            }
        }
        in.close();
    }
}

int Twitterak::usersSize()
{
    return this->users.size();
}

void Twitterak::save()
{
    this->saveProfiles();
    this->saveFollows();
    this->saveTweets();
}


void Twitterak::load()
{
    this->loadProfiles();
    this->loadFollows();
    this->loadTweets();
}
