// Tweet class definition; Member functions defined in Tweet.cpp

#ifndef TWEET_HPP
#define TWEET_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Date.hpp"
#include "Mention.hpp"
#include "Hashtag.hpp"

class User;
class Tweet
{
public:
    Tweet ();
    void setText(const std::string&);
    std::string getText();
    void setLike(User*);
    int getSize();
    void seDislike(User*);
    void showLike();
    bool checkLike(User*);
    Mention* setMention(std::string&, User*);
    void loadHashtags(User*, Tweet*);
    void saveLikers(std::ofstream&);
    void deleteTweet();
    void deleteMentions();
    void deleteHashtags();
    void setMention(Mention*);
private:
    std::vector <Mention*> mentionTweet;
    std::vector <User*> likes;
    std::vector <Hashtag*> hashtags;
    Date date;
    std::string text;
};

#endif // TWEET_HPPPP
