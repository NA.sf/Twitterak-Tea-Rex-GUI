// Twitterak class definition; Member functions defined in Twitterak.cpp

#ifndef TWITTERAK_HPP
#define TWITTERAK_HPP

#include <iostream>

#include "User.hpp"
#include "Tweet.hpp"

class Twitterak
{
public:
    static User* getUser(std::string = "", int = 0);
//    void run();
//    void help();
//    void helpLogin();
    User* checkLogin(const std::string&, const std::string&); // Check password and username
//    void login(User *);
    void editProfile(User*, const std::string& = "", const std::string& = "", const std::string& = "",
                                const std::string& = "", const std::string& = "", const std::string& = "",
                     const std::string& = "", const std::string& = "", const std::string& = "");
    User* signup(int, const std::string&, const std::string&, const std::string& = "", const std::string& = ""
                   , const std::string& = "", const std::string& = "", const std::string& = "", const std::string& = ""
                   , const std::string& = "", const int& = 1, const int& = 1, const int& = 1902);
    void deleteAccount(User*);
//    void showProfileMe(const User *) const;
//    void showProfile(const std::string &) const;

//    void editProfile(User*, const std::string&, std::string&);
    void save();
    void load();
    void saveProfiles();
    void loadProfiles();
    void saveTweets();
    void loadTweets();
    void saveFollows();
    void loadFollows();

    int usersSize();
private:
    static std::vector<User*> users;
};

#endif
