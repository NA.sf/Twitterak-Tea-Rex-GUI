#include "Hashtag.hpp"
#include "Tweet.hpp"
#include "User.hpp"
#include <string>
#include <vector>

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>

std::vector<Hashtag> Hashtag::hashtags;


Hashtag::Hashtag() {}

Hashtag::Hashtag(User* tweeter, Tweet* tweet, const std::string& hashtag) :
    hashtag(hashtag)
{
    tweeters.push_back(tweeter);
    tweets.push_back(tweet);
}


Hashtag* Hashtag::setHashtag(User* tweeter, Tweet* tweet, const std::string& hashtag)
{
    for (Hashtag& foundHashtag : hashtags) // search if the hashtag has been used earlier
    {
        if (foundHashtag.hashtag == hashtag) // if it has been used it would add the user and their tweet in the vectors of hashtag
        {
            foundHashtag.tweeters.push_back(tweeter);
            foundHashtag.tweets.push_back(tweet);
            return (&foundHashtag);
        }
    }
    // if the hashtag has not been used it will create the hashtag in vector of hashtags
    Hashtag newHashtag(tweeter, tweet, hashtag);
    hashtags.push_back(newHashtag);
    return &hashtags[hashtags.size() - 1];
}
Hashtag* Hashtag::checkHashtag(std::string check)
{
    for (Hashtag& foundHashtag : hashtags)
    {
        if (foundHashtag.hashtag == check)
        {
            return (&foundHashtag);
        }
    }
    return nullptr;
}

int Hashtag::getSizeOfVector()
{
    return tweeters.size();
}
std::string Hashtag::getTweet(int number)
{
    return tweets[number]->getText();
}
std::string Hashtag::getTweeter(int number)
{
    return tweeters[number]->getUsername();
}
Tweet* Hashtag::Tweets(int number)
{
    return this->tweets[number];
}

void Hashtag::deleteTweet(Tweet* deletingTweet)
{
    int pos{0};
    for (Tweet * tweet : this->tweets)
    {
        if (tweet == deletingTweet)
        {
            this->tweets.erase(this->tweets.begin() + pos);
            this->tweeters.erase(this->tweeters.begin() + pos);
            return;
        }
        pos++;
    }

}
