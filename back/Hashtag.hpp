#ifndef HASHTAG_HPP
#define HASHTAG_HPP

#include <string>
#include <vector>

class User;
class Tweet;

class Hashtag
{
public:
    Hashtag();
    Hashtag(User*, Tweet*, const std::string&);
    Hashtag* setHashtag(User*, Tweet*, const std::string&);
    Hashtag* checkHashtag(std::string);
    int getSizeOfVector();
    std::string getTweet(int);
    std::string getTweeter(int);
    Tweet* Tweets(int);
    void deleteTweet(Tweet*);
private:
    std::string hashtag;
    static std::vector <Hashtag> hashtags;
    std::vector <User*> tweeters;
    std::vector <Tweet*> tweets;
};

#endif // HASHTAG_HPPPP
