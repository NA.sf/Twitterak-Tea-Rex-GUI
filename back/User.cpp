// User class member-function definitions.

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <ctime>
#include <iomanip>
#include <QDebug>

#include "hash-library/sha256.h"
#include "User.hpp"
#include "Tweet.hpp"
#include "Date.hpp"
#include "Config.hpp"
#include "Mention.hpp"
#include "Twitterak.hpp"

User::User()
{

}

void User::setUsername(const std::string& input)
{
    std::string keyWords = input;
    std::transform(keyWords.begin(), keyWords.end(), keyWords.begin(), ::tolower);
    if (input.empty()) throw std::invalid_argument(" ! Username is required.");
    if (keyWords == "login" || keyWords == "signup" || keyWords == "username" || keyWords == "biography" ||
        keyWords == "country" || keyWords == "phonenumber" || keyWords == "password" || keyWords =="header" ||
        keyWords == "birthdate" || keyWords == "delete" || keyWords == "account" || keyWords == "profile" ||
        keyWords == "tweet" || keyWords == "dislike" || keyWords == "logout" || keyWords == "Anonymous User")
    {
        throw std::invalid_argument(" ! This is a reserved keyword.");
    }
    this->username = input;
}

void User::setID(const int& ID)
{
    this->ID = ID;
}

void User::setName(const std::string& input)
{
    if (input.empty()) throw std::invalid_argument(" ! Name is required.");
    if (input != "Anonymous User") this->name = input;
    else throw std::invalid_argument(" ! This is a reserved name.");
}

void User::setBiography(const std::string& input)
{
    this->biography = input;
}

void User::setCountry(const std::string& input)
{
    this->country = input;
}

void User::setLink(const std::string& input)
{
    if ((input.size() > 0) && (input.substr(0, 8) != "https://"))
    {
        this->link = "https://" + input;
    }
    else
    {
        this->link = input;
    }
}

void User::setPhoneNumber(const std::string& input)
{
    if (input.empty()) throw std::invalid_argument(" ! Phone number is required.");
    this->phoneNumber = input;
}

void User::setPassword(const std::string& input)
{
    if (input.empty()) throw std::invalid_argument(" ! Password is required.");
    if (input == "123456" || input == "654321" || input == "account" || input == "Tea-Rex" || input.length() < 6)
    {
        throw std::invalid_argument(" ! Password is too weak.");
    }

    SHA256 sh;
    if (!this->password.empty())
    {
        for (const std::string& previousPassword : this->previousPasswords)
        {
            if (sh(input) == previousPassword) std::invalid_argument(" ! This password is the same as your old password. Enter a new one.");
        }
        this->previousPasswords.push_back(sh(this->password));
    }
    this->password = sh(input);
}

void User::setHeader(const std::string& input)
{
//    std::transform(input.begin(), input.end(), input.begin(), ::tolower);
//    if (input == "white" || input == "red" || input == "orange" || input == "yellow" || input == "pink" ||
//        input == "green" || input == "blue" || input == "purple" || input == "black")
//    {
        this->header = input;
//    }
//    else if (input == "")
//    {
//        this->header = "white";
//    }
//    else
//    {
//        throw std::invalid_argument("\t! The header is not valid!");
//    }
}

void User::setBirthDay(int dd)
{
    birthDate.setDay(dd);
}

void User::setBirthMonth(int mm)
{
    birthDate.setMonth(mm);
}

void User::setBirthYear(int yy)
{
    birthDate.setYear(yy);
}


void User::setFilePhone(const std::string& phoneNumber)
{
    this->phoneNumber = phoneNumber;
}

void User::setBirthDate(const std::string& date)
{
    int day;
    std::stringstream ss;
    std::string month = date.substr(0, 3);
    ss << date.substr(4, date.size() - 4);
    ss >> day;
    this->setBirthDay(day);
    this->setBirthYear(stoi(date.substr(date.find(",") + 2, 4)));
    if (month == "Jan") this->setBirthMonth(1);
    else if (month == "Feb") this->setBirthMonth(2);
    else if (month == "Mar") this->setBirthMonth(3);
    else if (month == "Apr") this->setBirthMonth(4);
    else if (month == "May") this->setBirthMonth(5);
    else if (month == "Jun") this->setBirthMonth(6);
    else if (month == "Jul") this->setBirthMonth(7);
    else if (month == "Aug") this->setBirthMonth(8);
    else if (month == "Sep") this->setBirthMonth(9);
    else if (month == "Oct") this->setBirthMonth(10);
    else if (month == "Nov") this->setBirthMonth(11);
    else if (month == "Dec") this->setBirthMonth(12);
}

std::string User::getUsername() const
{
    return this->username;
}

std::string User::getName() const
{
    return this->name;
}

std::string User::getBiography() const
{
    return this->biography;
}

std::string User::getCountry() const
{
    return this->country;
}

std::string User::getLink() const
{
    return this->link;
}

std::string User::getPhoneNumber() const
{
    return phoneNumber;
}

std::string User::getPassword() const
{
    return this->password;
}

std::string User::getHeader() const
{
    return this->header;
}

std::string User::getBirthDate() const
{
    return this->birthDate.printDate();
}

std::string User::showColor() const
{
//    std::string Header = this->header;
//    if (Header == "red")
//    {
//        return RED;
//    }
//    else if (Header == "orange")
//    {
//        return ORANGE;
//    }
//    else if (Header == "yellow")
//    {
//        return YELLOW;
//    }
//    else if (Header == "pink")
//    {
//        return PINK;
//    }
//    else if (Header == "green")
//    {
//        return GREEN;
//    }
//    else if (Header == "blue")
//    {
//        return BLUE;
//    }
//    else if (Header == "purple")
//    {
//        return PURPLE;
//    }
//    else if (Header == "black")
//    {
//        return BLACK;
//    }
//    return "";
}

void User::deleteTweet(int num)
{
    this->vecTweet[num] = nullptr;
}

void User::changeTweet(int num, std::string& str)
{ 
    vecTweet[num - 1]->setText(str); 
}

std::vector<Tweet *> User::getVecTweet()
{
    return this->vecTweet;
}

Tweet * User::getLastTweet()
{
    return this->vecTweet[this->vecTweet.size() - 1];
}

void User::setVec(const std::string& st)
{
//    qDebug() << "here in setVec() for " << this->username << st;
    if (st == " ") this->vecTweet.push_back(nullptr);
    else
    {
        Tweet* newTweet = new Tweet();
        newTweet->setText(st);
        newTweet->loadHashtags(this, newTweet); // load the hashtags for the tweet that has just added
        this->vecTweet.push_back(newTweet);
    }
}

void User::deleteTweets()
{
    for (size_t i{0}; i < this->vecTweet.size(); ++i)
    {
        this->vecTweet[i]->deleteTweet(); // clear the tweet's data (mention and hashtag)
        delete this->vecTweet[i]; // deallocate the memory
    }
    this->vecTweet.clear(); // clears the vector
}

void User::setWork(User* work)
{
    this->work = work;
}
User* User::getWork()
{
    return this->work;
}

int User::getID()
{
    return this->ID;
}

void User::saveProfile(std::ofstream& out, std::string&)
{
    out << this->ID << '\n' << this->username << '\n' << this->password << '\n';
}

void User::loadProfile(std::ifstream& in)
{
    in.ignore();
    in >> this->ID;
    in.ignore();
    getline(in, this->username);
    getline(in, this->password);
}


void User::savePreviousPasswords(std::ofstream& out)
{
    for (std::string& previousPassword : this->previousPasswords)
    {
        out << previousPassword << '-';
    }
    out << '\n';
}

void User::loadPreviousPasswords(std::ifstream& in)
{
    std::string stringPassword;
    getline(in, stringPassword);
    std::stringstream passwordSplit(stringPassword);
    while(getline(passwordSplit, stringPassword, '-'))
    {
        this->previousPasswords.push_back(stringPassword);
    }
}

void User::saveTweets(std::ofstream& out)
{
    for (size_t i{0}; i < this->vecTweet.size(); ++i) // write the user's tweets
    {
        if (this->vecTweet[i] != nullptr) // check if it's a deleted tweet or not
        {
            out << i + 1 << '.' << this->vecTweet[i]->getText() << '~'; // write the tweet's ID and text
            this->vecTweet[i]->saveLikers(out); // write likes of the tweet in file
        }
    }
}

bool User::hasTweets() const
{
    return this->vecTweet.size();
}

bool User::hasFollowers() const
{
    return this->followers.size();
}

bool User::hasFollowings() const
{
    return this->followings.size();
}

void User::setFollowerDirectly(User* user)
{
    this->followers.push_back(user);
}

void User::setFollowingDirectly(User* user)
{
    this->followings.push_back(user);
}

void User::setFollower(User* user)
{
    int pos = 0;
    for (User * follower : this->followers)
    {
        if (user == follower)
        {
            this->followers.erase(this->followers.begin() + pos);
            return;
        }
        ++pos;
    }
    this->followers.push_back(user);
}

void User::setFollowing(User* user)
{
    int pos = 0;
    for (User * followed : this->followings)
    {
        if (user == followed)
        {
            this->followings.erase(this->followings.begin() + pos);
            return;
        }
        ++pos;
    }
    this->followings.push_back(user);
}

void User::saveFollowers(std::ofstream& out)
{
    for (User* follower : this->followers)
    {
        out << follower->getID() << '-';
    }
}

void User::saveFollowings(std::ofstream& out)
{
    for (User* following : this->followings)
    {
        out << following->getID() << '-';
    }
}

std::vector <User*> User::getFollowers()
{
    qDebug() << "Here in getFollowers";
    return this->followers;
}


std::vector <User*> User::getFollowings()
{
    qDebug() << "Here in getFollowings";
    return this->followings;
    qDebug() << "Here after getFollowings";
}

int User::followersCount()
{
    return this->followers.size();
}

int User::followingsCount()
{
    return this->followings.size();
}

void User::deleteFollows()
{
    for (User* followed : this->followings)
    {
        followed->setFollower(this);
    }
    for (User* follower : this->followers)
    {
        follower->setFollowing(this);
    }
}
