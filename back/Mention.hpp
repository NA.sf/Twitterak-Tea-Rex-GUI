#ifndef MENTION_HPP
#define MENTION_HPP

//#include <iostream>
#include <string>
#include <vector>
#include <fstream>

class User;

class Mention
{
public:
    Mention(std::string, User *);
    //void setMention(std::string &, User *);
    User* getMentioner();
    std::string getMentionText();
    void setLike(User*);
    void saveLikers(std::ofstream&);

private:
    std::string mentionText;
    User* mentioner;
    std::vector <User*> likes;

};

#endif
