// User class definition; Member functions defined in User.cpp

#ifndef ANONYMOUS_HPP
#define ANONYMOUS_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Tweet.hpp"
#include "Date.hpp"
#include "Mention.hpp"
#include "User.hpp"


class Anonymous : public User
{
using User::User;
public:
    void setName(const std::string&) override;
    void setBiography(const std::string&) override;
    void setCountry(const std::string&) override;
    void setLink(const std::string&) override;
    void setPhoneNumber(const std::string&) override;
    void setHeader(const std::string&) override;
    void setBirthDay(int) override;
    void setBirthMonth(int) override;
    void setBirthYear(int) override;
    void setWork(User*) override;
    void setFilePhone(const std::string&) override;

    std::string getBirthDate() const override;
    std::vector<Tweet *> getVecTweet() override; // User's tweets
    void deleteTweet(int) override;
    void changeTweet(int, std::string&) override;
    void setVec(const std::string&) override; // Adds new tweet
    void deleteTweets() override;

    void setFollower(User*) override;

    void saveProfile(std::ofstream&, std::string&) override;
    void loadProfile(std::ifstream&) override;
//    void loadProfile(const std::string&) override;
//    void saveTweets(std::ofstream&) override;
//    User* getWork()override;
};

#endif
