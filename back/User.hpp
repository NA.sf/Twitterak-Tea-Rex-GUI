// User class definition; Member functions defined in User.cpp

#ifndef USER_HPP
#define USER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Tweet.hpp"
#include "Date.hpp"
#include "Mention.hpp"

class User
{
public:
    User();
    User(int, int, int);
    virtual ~User() {}
    virtual void setID(const int&);
    virtual void setUsername(const std::string&);
    virtual void setName(const std::string&);
    virtual void setBiography(const std::string&);
    virtual void setCountry(const std::string&);
    virtual void setLink(const std::string&);
    virtual void setPhoneNumber(const std::string&);
    virtual void setPassword(const std::string&);
    virtual void setHeader(const std::string&);
    virtual void setBirthDay(int);
    virtual void setBirthMonth(int);
    virtual void setBirthYear(int);
    virtual void setWork(User*);
    virtual void setFilePhone(const std::string&);
    virtual void setBirthDate(const std::string&);

    virtual std::string getUsername() const;
    virtual std::string getName() const;
    virtual std::string getBiography() const;
    virtual std::string getCountry() const;
    virtual std::string getLink() const;
    virtual std::string getPhoneNumber() const;
    virtual std::string getPassword() const;
    virtual std::string getHeader() const;
    virtual std::string getBirthDate() const;
    virtual User* getWork();

    int getID();

    virtual std::string showColor() const; // for showing header's color
    virtual std::vector<Tweet *> getVecTweet(); // User's twwets
    virtual void deleteTweet(int);
    virtual void changeTweet(int, std::string&);
    virtual void setVec(const std::string&); // Adds new tweet
    Tweet * getLastTweet();
    virtual void deleteTweets();

    virtual void saveProfile(std::ofstream&, std::string&);
    virtual void loadProfile(std::ifstream&);
    virtual void savePreviousPasswords(std::ofstream&);
    virtual void loadPreviousPasswords(std::ifstream&);

    virtual void saveTweets(std::ofstream&);
    virtual bool hasTweets() const;

    virtual void setFollowerDirectly(User*);
    virtual void setFollowingDirectly(User*);
    virtual void setFollower(User*);
    virtual void setFollowing(User*);
    virtual bool hasFollowers() const;
    virtual bool hasFollowings() const;
    virtual void saveFollowers(std::ofstream&);
    virtual void saveFollowings(std::ofstream&);

    virtual std::vector <User*> getFollowers();
    virtual std::vector <User*> getFollowings();

    virtual int followersCount();
    virtual int followingsCount();

    virtual void deleteFollows();

private:
    std::string name = "Anonymous User", username = "", biography = "", country = "", link = "", phoneNumber = "", password = "", header = "";
    int ID;
    Date birthDate;
    std::vector <Tweet*> vecTweet;
    std::vector <User*> followers;
    std::vector <User*> followings;
    std::vector <std::string> previousPasswords;
//    const Date joinDate;
    User* work{nullptr};
};

#endif
