#include "mainwindow.hpp"

#include <QApplication>
#include <QFont>

#include "back/Twitterak.hpp"

int main(int argc, char *argv[])
{
    Twitterak * twitterak = new Twitterak;
    twitterak->loadProfiles();
    twitterak->loadFollows();
    twitterak->loadTweets();
    QApplication a(argc, argv);
    int fontId = QFontDatabase::addApplicationFont(":/img/img/FiraSansRegular.ttf");
    QString family = QFontDatabase::applicationFontFamilies(fontId).at(0);
    QFont defaultFont(family,14,QFont::Normal);
    a.setFont(defaultFont);
    MainWindow w(nullptr, twitterak);
    w.show();
    return a.exec();
}
