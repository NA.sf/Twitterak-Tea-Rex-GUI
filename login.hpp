#ifndef LOGIN_HPP
#define LOGIN_HPP

#include <QMainWindow>
#include <QCloseEvent>
#include "back/Twitterak.hpp"

namespace Ui {
class Login;
}

class MainWindow;
class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = nullptr, MainWindow *mainWindow = nullptr, Twitterak *twitterak = nullptr);
    void changeStyleFilled(const QString &text);
    ~Login();

private slots:
    void on_btnBack_clicked();

    void on_btnLogin_clicked();

protected:
    void closeEvent(QCloseEvent *) override;

private:
    Ui::Login *ui;
    MainWindow *mainWindow;
    Twitterak *twitterak;
};

#endif // LOGIN_HPP
