#include "clickablelabel.hpp"
#include <QDebug>


ClickableLabel::ClickableLabel(QWidget* parent) : QLabel(parent)
{
    setCursor(Qt::PointingHandCursor);
}


ClickableLabel::~ClickableLabel()
{

}
void ClickableLabel::mousePressEvent(QMouseEvent* event)
{
    qDebug() << "Label clicked";
    emit clicked();
}
