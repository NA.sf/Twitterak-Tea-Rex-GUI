#include "login.hpp"
#include "ui_login.h"

#include "mainwindow.hpp"
#include "loggedinwindow.hpp"
#include "back/Config.hpp"

#include <QRegularExpressionValidator>
#include <QMessageBox>
#include <QDebug>
#include <iostream>

Login::Login(QWidget *parent, MainWindow *mainWindow, Twitterak *twitterak) :
    QMainWindow(parent),
    ui(new Ui::Login),
    mainWindow(mainWindow),
    twitterak(twitterak)
{
    ui->setupUi(this);
    connect(ui->toolButton, &QToolButton::toggled, this, [this](bool checked)
    {
        if(checked)
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeClose.png"));
            ui->linePassword->setEchoMode(QLineEdit::Normal);
        }
        else
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeOpen.png"));
            ui->linePassword->setEchoMode(QLineEdit::Password);
        }
    });

    ui->lineUsername->setValidator(new QRegularExpressionValidator(QRegularExpression("[A-Za-z][A-Za-z0-9]*"), this)); // validates username
    connect(ui->linePassword, &QLineEdit::textChanged, this, &Login::changeStyleFilled);
    connect(ui->lineUsername, &QLineEdit::textChanged, this, &Login::changeStyleFilled);
}

Login::~Login()
{
    qDebug() << "here for signup";
    delete ui;
    delete twitterak;
}

void Login::closeEvent(QCloseEvent *)
{
    this->hide(); // Hide the current window
    this->mainWindow->show();
}

void Login::on_btnBack_clicked()
{
    this->hide();
    this->mainWindow->show();
}


void Login::on_btnLogin_clicked()
{
    if (this->ui->lineUsername->text().isEmpty())
    {
        setErrorStyle(" This feild is required!", this->ui->lineUsername);
    }
    else if (this->ui->lineUsername->text().length() < 5)
    {
        setErrorStyle(" This feild is required!", this->ui->lineUsername);
    }
    if (this->ui->linePassword->text().isEmpty())
    {
        setErrorStyle(" This feild is required!", this->ui->linePassword, this->ui->passwordBox);
    }
    else
    {
        std::string username = ui->lineUsername->text().toStdString();
        std::string password = ui->linePassword->text().toStdString();
        try
        {
            User* currentUser = new User();
            currentUser = this->twitterak->checkLogin(username, password);
            LoggedInWindow* loggedInWindow = new LoggedInWindow(nullptr, currentUser);
            loggedInWindow->show();
            this->close();
            this->mainWindow->close();
            QMessageBox::information(this, "Well Done!", " * You have succcessfully logged in.");
        }
        catch (const std::invalid_argument & ex)
        {
            if(std::string(ex.what()) == std::string(" ! Password is not correct."))
            {
                setErrorStyle(" ! Password is not correct.", this->ui->linePassword, this->ui->passwordBox);
            }
            else if(std::string(ex.what()) == std::string(" ! User doesn't exist."))
            {
                setErrorStyle(" ! User doesn't exist.", this->ui->lineUsername);
            }
        }
    }
}

void Login::changeStyleFilled(const QString &text)
{
    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(sender());
    if (lineEdit) {
        lineEdit->setStyleSheet("border: 2px solid black; border-radius: 20px; color: rgb(0, 0, 0);");
    }
    if (lineEdit == ui->linePassword)
    {
        ui->linePassword->setStyleSheet("border: none; border-radius: 0px; background-color: rgba(255, 255, 255, 0);color: rgb(0, 0, 0);");
        ui->passwordBox->setStyleSheet("border: 2px solid black; border-radius: 20px;");
    }
    lineEdit->setPlaceholderText("");
}
