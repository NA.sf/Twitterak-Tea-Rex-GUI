#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <iostream>
#include <QDebug>
//#include "signup.hpp"
//#include "login.hpp"

MainWindow::MainWindow(QWidget *parent, Twitterak * twitterak)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , twitterak(twitterak)
{
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/img/img/Tea-Rex.png"));
//    twitterak->loadProfiles();
    ui->groupBox->move((width() - ui->groupBox->width()) / 2, (height() - ui->groupBox->height()) / 2);
    ui->lblTwitter->move(0, 0);
    loginUi = new Login(parent, this);
    signupUi = new Signup(parent, this);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    int lblTwitterX = (ui->groupBox->width() - this->ui->lblTwitter->width()) / 2;
    this->ui->lblTwitter->move(lblTwitterX, 30);
    int btnX = (ui->groupBox->width() - this->ui->btnSignup->width()) / 2;
    this->ui->btnSignup->move(btnX, 25 + ui->lblTwitter->y() + ui->lblTwitter->height());
    this->ui->btnLogin->move(btnX, 50 + ui->lblTwitter->y() + ui->lblTwitter->height() + ui->btnSignup->height());
    QMainWindow::resizeEvent(event);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_btnLogin_clicked()
{
    hide();
    this->loginUi->show();
}

void MainWindow::on_btnSignup_clicked()
{
    hide();
    this->signupUi->show();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
//    std::cout << "\nin main close\n";

//    event->ignore();
//    qDebug() << "\ninmain close\n";
//    this->close();
//    twitterak->loadProfiles();
//    twitterak->loadTweets();
//    twitterak->loadFollows();
    QMainWindow::closeEvent(event);
}

