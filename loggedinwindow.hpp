#ifndef LOGGEDINWINDOW_HPP
#define LOGGEDINWINDOW_HPP

#include <QMainWindow>
#include<QLabel>
#include <QToolButton>

#include "profile.hpp"
#include "back/User.hpp"
#include "mytextedit.hpp"
#include "back/Twitterak.hpp"
#include "back/Hashtag.hpp"
//#include "settingwindow.hpp"
#include "mainwindow.hpp"

namespace Ui {
class LoggedInWindow;
}

class LoggedInWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoggedInWindow(QWidget *parent = nullptr, User* currentUser = nullptr, Twitterak* twitterak = nullptr, Hashtag* hashtag = nullptr);
    void closeEvent(QCloseEvent *event) override;
    void updateFollow();
    ~LoggedInWindow();

private slots:

//    void on_pushButton_clicked();
//    void onButtonClicked();//for search bar

//    void on_pushButton_hashtag_clicked();
    //void search_hashtag();// for search the #

    void new_Window();

    bool eventFilter(QObject *watched, QEvent *event) override;

    void on_btnTweet_clicked();
    void on_btnSearchHashtag_clicked();

    void on_btnSearchAccount_clicked();

public slots:
    void like(bool);

//    void on_pushButton_2_clicked();

    void on_btnSetting_clicked();

private:
    QVector<QToolButton*> buttons;
    Ui::LoggedInWindow *ui;
//    MainWindow * mainWindow;
    User* currentUser;
    Twitterak* twitterak;
//    Profile * another/* = new Profile*/;
    Hashtag* hashtag;
    MyTextEdit* tweetTextEdit;
//    SettingWindow *settingwWindow;
};


#endif // LOGGEDINWINDOW_HPP
