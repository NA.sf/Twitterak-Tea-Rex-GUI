#include "toolbuttons.hpp"

ToolButtons::ToolButtons(QWidget* parent, Tweet* tweet, Mention* mention) :
    QToolButton(parent),
    tweet(tweet),
    mention(mention)
{
}
Mention* ToolButtons::getMention()
{
    return this->mention;
}
Tweet* ToolButtons::getTweet()
{
    return this->tweet;
}
