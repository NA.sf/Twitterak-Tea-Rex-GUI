// a subclass of QTextEdit with needed features for Twitterak

#ifndef MYTEXTEDIT_HPP
#define MYTEXTEDIT_HPP

#include <QPlainTextEdit>
#include <QMimeData>

class MyTextEdit : public QTextEdit
{
    Q_OBJECT

public:
    explicit MyTextEdit(QWidget *parent = nullptr, int limitation = 0, bool isTweet = false);
    void setLimitation(int);
    int getLimitation();
    void adjustHeight();
    void fixText();

protected:
    void keyPressEvent(QKeyEvent *e) override; // for limiting user's biography length
    void insertFromMimeData(const QMimeData* source) override; // for limiting user's length of pasted text for biography
    void mouseReleaseEvent(QMouseEvent *event) override;


    //    void paintEvent(QPaintEvent *event) override;

private:
    int MAX_LENGTH = 0;
    bool isTweet = false;
};

#endif // MYTEXTEDIT_HPP
