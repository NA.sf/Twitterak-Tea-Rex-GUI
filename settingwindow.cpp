#include "settingwindow.hpp"
#include "ui_settingwindow.h"

#include "loggedinwindow.hpp"
#include "mainwindow.hpp"

#include "back/Config.hpp"

#include "back/User.hpp"


#include <QFileDialog>
#include <QBitmap>
#include <QPainter>
#include <QPainterPath>
#include <QScrollArea>
#include <QScrollBar>
#include <QDebug>
#include <QLineEdit>
#include <QRegularExpressionValidator>
#include <QGuiApplication>
#include <QScreen>
#include <QVBoxLayout>
#include <QMessageBox>
#include <iostream>

SettingWindow::SettingWindow(QWidget *parent, /*LoggedInWindow *loggedInWindow,*/ Twitterak *twitterak, User* currentUser) :
    QMainWindow(parent),
    ui(new Ui::SettingWindow),
    /*loggedInWindow(loggedInWindow),*/
    twitterak(twitterak),
    currentUser(currentUser)

{
    ui->setupUi(this);
    setFixedHeight(QGuiApplication::primaryScreen()->geometry().height() - 100);
    QWidget *widget = new QWidget();
    widget->setLayout(new QVBoxLayout);
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->groupBox->setLayout(new QVBoxLayout);
    bioTextEdit = new MyTextEdit(ui->groupBox);
    widget->layout()->addWidget(ui->groupBox);
    ui->scrollArea->setWidget(widget);
    ui->scrollArea->verticalScrollBar()->setStyleSheet(
        "QScrollBar:vertical{border: none;background-color: rgb(164, 195, 178);width: 8px;margin: 0px 0 0px 0;border-radius: 0px; font-color: black;}"
        "QScrollBar::handle:vertical{background-color: rgb(107, 144, 128);min-height: 30px;border-radius: 4px;}"
        "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical, QScrollBar::add-line:vertical, QScrollBar::sub-line:vertical {width: 0px;}");
    ui->scrollArea->setStyleSheet("border: none;");
    connect(ui->toolButton, &QToolButton::toggled, this, [this](bool checked)
    {
        if(checked)
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeClose.png"));
            ui->linePassword->setEchoMode(QLineEdit::Normal);
        }
        else
        {
            ui->toolButton->setIcon(QIcon(":/img/img/eyeOpen.png"));
            ui->linePassword->setEchoMode(QLineEdit::Password);
        }
    });
    ui->comboHeader->setItemData(0, QColor(255, 255, 252), Qt::ForegroundRole);
    ui->comboHeader->setItemData(1, QColor(  0,   0,   3), Qt::ForegroundRole);
    ui->comboHeader->setItemData(2, QColor(255, 214, 165), Qt::ForegroundRole);
    ui->comboHeader->setItemData(3, QColor(255, 198, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(4, QColor(255, 173, 173), Qt::ForegroundRole);
    ui->comboHeader->setItemData(5, QColor(155, 246, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(6, QColor(202, 255, 191), Qt::ForegroundRole);
    ui->comboHeader->setItemData(7, QColor(189, 178, 255), Qt::ForegroundRole);
    ui->comboHeader->setItemData(8, QColor(253, 255, 182), Qt::ForegroundRole);
    bioTextEdit->setFixedSize(429, 151);
    bioTextEdit->setWordWrapMode(QTextOption::WrapAnywhere);
    bioTextEdit->setStyleSheet("QTextEdit:enabled{background-color: rgb(246, 255, 248); border: 2px solid black; border-radius: 20px;}"
                               "QTextEdit:disabled{background-color: rgb(246, 255, 248); border: 2px solid gray; border-radius: 20px;}");
    ui->profile->installEventFilter(this);
    ui->linePhone->setValidator(new QRegularExpressionValidator(QRegularExpression("^989[0-9]{9}"), this)); // validates phone number to accepts 989xxxxxxxxx
    ui->lineUsername->setValidator(new QRegularExpressionValidator(QRegularExpression("[A-Za-z][A-Za-z0-9]*"), this)); // validates username

    bioTextEdit->move((ui->groupBox->width() - bioTextEdit->width()) / 2, ui->unnecessaryLines->y() + ui->unnecessaryLines->height());

    connect(ui->linePassword, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->lineUsername, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->lineName, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->linePhone, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->lineWork, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->lineCountry, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);
    connect(ui->lineLink, &QLineEdit::textChanged, this, &SettingWindow::changeStyleFilled);

    ui->lineUsername->setText(QString::fromStdString(this->currentUser->getUsername()));
    ui->lineName->setText(QString::fromStdString(this->currentUser->getName()));
    ui->linePhone->setText(QString::fromStdString(this->currentUser->getPhoneNumber()));
    ui->lineLink->setText(QString::fromStdString(this->currentUser->getLink()));
    if (this->currentUser->getWork() != nullptr) ui->lineWork->setText(QString::fromStdString(this->currentUser->getWork()->getUsername()));
    ui->lineCountry->setText(QString::fromStdString(this->currentUser->getCountry()));
    bioTextEdit->setText(QString::fromStdString(this->currentUser->getBiography()));
    if (currentUser->getName() == "Anonymous User") this->ui->profile->setPixmap(QPixmap(":/img/img/anonymous.png"));
    else if(QFile::exists("../Files/Profiles/" + QString::number(this->currentUser->getID())))
    {
        profile = "../Files/Profiles/" + QString::number(this->currentUser->getID());
        this->eventFilter(this->ui->profile, new QEvent(QEvent::Shortcut));
    }
}

SettingWindow::~SettingWindow()
{
    delete ui;
}

void SettingWindow::closeEvent(QCloseEvent *)
{
    if (this->currentUser != nullptr)
    {
        LoggedInWindow * loggedInWindow = new LoggedInWindow(nullptr, this->currentUser, this->twitterak);
        loggedInWindow->show();
    }
    else this->twitterak->save();
    this->close(); // close the current window
}

bool SettingWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == this->ui->profile && (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::Shortcut))
    {
        if (this->currentUser->getName() == "Anonymous User") QMessageBox::critical(this, "Error!", " ! Access Denied: You do not have permission to modify this.");
        else
        {
            if (event->type() == QEvent::MouseButtonPress) profile = QFileDialog::getOpenFileName(this, tr("Open Image"), "../", tr("Image Files (*.png *.jpg *.bmp)"));
            if (!profile.isEmpty())
            {
                if (!QDir("../Files/Profiles/").exists())
                {
                    QDir("../Files/Profiles/").mkpath(".");
                }
                QPixmap pixmap(profile);
                int size = qMin(pixmap.width(), pixmap.height());
                pixmap = pixmap.copy((pixmap.width() - size) / 2, (pixmap.height() - size) / 2, size, size);
                QBitmap mask(pixmap.size());
                QPainter painter(&mask);
                painter.fillRect(mask.rect(), Qt::color0);
                painter.setBrush(Qt::color1);
                painter.drawEllipse(mask.rect());
                pixmap.setMask(mask);
                pixmap = pixmap.scaled(ui->profile->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
                this->ui->profile->setPixmap(pixmap);
            }
        }
        return true;
    }
    else
    {
        return QObject::eventFilter(obj, event);
    }
}

void SettingWindow::changeStyleFilled(const QString &)
{
    QLineEdit *lineEdit = qobject_cast<QLineEdit*>(sender());
    if (lineEdit) {
        lineEdit->setStyleSheet("QLineEdit:disabled{border: 2px solid gray; border-radius: 20px; font-color: gray;}"
                                "QLineEdit:enabled{border: 2px solid black; border-radius: 20px; font-color: black;}");
    }
    if (lineEdit == ui->linePassword)
    {
        this->ui->linePassword->setStyleSheet("border: none; border-radius: 0px; background-color: rgba(255, 255, 255, 0); color: rgb(0, 0, 0);");
        this->ui->passwordBox->setStyleSheet("border: 2px solid black; border-radius: 20px;");
    }
    lineEdit->setPlaceholderText(NULL);
    if (lineEdit == ui->linePhone)
    {
        this->ui->linePhone->setPlaceholderText(" (must start with 989, 12 characters)");
    }
}



void SettingWindow::on_btnLoggedOut_clicked()
{
    MainWindow * mainWindow = new MainWindow;
    mainWindow->show();
    this->currentUser = nullptr;
    this->close();
}


void SettingWindow::on_btnDelete_clicked()
{
    int answer = QMessageBox::question(this, "Warning!", "This operation cannot be reversed in any away. Are you sure?");
    if (answer == QMessageBox::Yes)
    {
        this->twitterak->deleteAccount(this->currentUser);
        MainWindow * mainWindow = new MainWindow;
        mainWindow->show();
        this->currentUser = nullptr;
        this->close();
    }
}


void SettingWindow::on_btnEdit_clicked()
{
    try
    {
        this->twitterak->editProfile(this->currentUser, this->ui->lineUsername->text().toStdString(),
                                this->ui->linePassword->text().toStdString(), this->ui->lineName->text().toStdString(),
                                this->ui->linePhone->text().toStdString(), this->ui->lineWork->text().toStdString(),
                                this->ui->lineCountry->text().toStdString(), this->ui->lineLink->text().toStdString(),
                                this->bioTextEdit->toPlainText().toStdString(), ui->comboHeader->currentText().toStdString());
        if (!profile.isEmpty())
        {
            if(QFile::exists("../Files/Profiles/" + QString::number(this->currentUser->getID())))
            {
                QFile::remove("../Files/Profiles/" + QString::number(this->currentUser->getID()));
            }
            QFile::copy(profile, "../Files/Profiles/" + QString::number(this->currentUser->getID()));
        }
        QMessageBox::information(this, "Update successful!", "Your information has been changed.");
    }
    catch (const std::invalid_argument& ex)
    {
        QMessageBox::critical(this, "Error!", ex.what());
    }
}

