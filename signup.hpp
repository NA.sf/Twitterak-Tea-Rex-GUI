#ifndef SIGNUP_HPP
#define SIGNUP_HPP

#include <QMainWindow>
#include <QLineEdit>
#include <QGroupBox>

#include "mytextedit.hpp"

#include "back/Twitterak.hpp"

namespace Ui {
class Signup;
}

class MainWindow;

class Signup : public QMainWindow
{
    Q_OBJECT

public:
    explicit Signup(QWidget *parent = nullptr, MainWindow *mainWindow = nullptr, Twitterak *twitterak = nullptr);
    void changeStyleFilled(const QString &text);
//    void setErrorStyle(const QString&, QLineEdit* line = nullptr, QGroupBox* group = nullptr);
    ~Signup();

private:
    Ui::Signup *ui;
    MyTextEdit * bioTextEdit;
    MainWindow *mainWindow; // for when user wants to go back to main window
    Twitterak *twitterak;
    QString profile{""};

protected:
    bool eventFilter(QObject *obj, QEvent *event) override; // for setting profile

    void closeEvent(QCloseEvent *) override;
//    void keyPressEvent(QKeyEvent *e) override;

private slots:
    void handleIndexChanged(int index); // limits datas needed to the user type
    void on_btnSignup_clicked();
};

#endif // SIGNUP_HPP
