#ifndef TOOLBUTTONS_HPP
#define TOOLBUTTONS_HPP

#include "back/Tweet.hpp"
#include "back/Mention.hpp"
#include <QToolButton>

class ToolButtons : public QToolButton
{
    Q_OBJECT
public:
    ToolButtons(QWidget* parent = nullptr,Tweet* tweet = nullptr, Mention* mention = nullptr);
private:
    Tweet* tweet;
    Mention* mention;
public:
   Mention* getMention();
   Tweet* getTweet();
};

#endif // TOOLBUTTONS_HPP
